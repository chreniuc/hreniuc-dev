---
slug: upgrading-the-version-of-rke2-cluster
title: Upgrading the version of rke2 cluster
authors: [cristi]
tags: [kubernetes, rke, rke2, upgrade, rancher ]
date: 2023-01-06T03:01
---

# Upgrading the version of rke2 cluster

Based on [this doc](https://docs.rke2.io/upgrade/basic_upgrade/), check [here](https://github.com/rancher/rke2/tags) the new versions

```bash

# Hetzner
ssh -i ~/.ssh/ansible_noob_id_rsa ansible_noob@SERVER-1
ssh -i ~/.ssh/ansible_noob_id_rsa ansible_noob@SERVER-2

sudo su -
curl -sfL https://get.rke2.io | INSTALL_RKE2_VERSION=v1.22.10+rke2r2 sh -

# For server
systemctl restart rke2-server 

# For agent
systemctl restart rke2-agent
```


```bash
# Check the logs
journalctl -u rke2-server -f
# Errors:
journalctl -u rke2-server | grep -i level=error

journalctl -u rke2-agent -f
journalctl -u rke2-agent | grep -i level=error

# See if pods are restarting or not working
kubectl get pod --all-namespaces
```

**Sometimes you might need to restart the backends, because this might trigger a restart for the vitess, and the backend sometimes doesn't refresh the connection to the db. Check the logs of the backend.**