---
slug: git-corruption-fix
title: Git corruption fix
authors: [cristi]
tags: [corruption, git]
date: 2020-08-04T19:16
---

# Git corruption fix

I got this error:

```bash
⬤ quotes_api ⑂chreniuc_351 ♦ git commit -m "added rule 2.1, 3.2"
error: inflate: data stream error (incorrect header check)
error: unable to unpack 2429559f950ba19b0ce5a9aeedd87820305f36b6 header
fatal: loose object 2429559f950ba19b0ce5a9aeedd87820305f36b6 (stored in .git/objects/24/29559f950ba19b0ce5a9aeedd87820305f36b6) is corrupt
[2]+  Done                    gitk --all
⬤ quotes_api ⑂chreniuc_351 ♦ git status
```

I ran `git fsck --full` and removed all objects that were corupted, but nothing seemed to work.

I've recloned the repository in a different folder and did the following:

```bash
$ cd ..
$ git clone someremote newsrc
$ cd src
$ mv .git /tmp/bad-git
$ mv ../newsrc/.git .
$ rm -rf ../newsrc
```

Explaination: I've used the `.git` folder of a valid git repository. The changes of the files that were not commited were not lost, they appeared as changed. And then I added them and created a commit.

Drawbacks:
 - you will lose all local branches, when you use the .git folder
 - you will lose all local commits that were on your current branch and were not pushed on the remote. The changes will remain, they will apear as changes that were not added to a commit.

[Source](https://wincent.com/wiki/Dealing_with_Git_repo_corruption)