---
slug: debug-executable-using-strace
title: Debug executable using strace
authors: [cristi]
tags: [c++, debug, executable, strace, trace]
date: 2019-02-14T19:04
---

# Debug executable using strace

How to debug using strace:
```bash
strace -s 80 -f -p 24274 2>&1 | stdbuf -i0 -o0 -e0 grep write
```

- `-s number` - Strace usually prints only a few chars for each system call, to make it print more chars use `-s number of chars`
- `-f` - Follow all threads opened by parent pid
- `-p pid` - Pid to follow
- `2>&1` - Reddirect `stderr` to `stdin` to be able to grep it
- `stdbuf -i0 -o0 -e0` - Flush each line to grep