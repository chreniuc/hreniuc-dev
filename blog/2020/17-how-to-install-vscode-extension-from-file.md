---
slug: how-to-install-vscode-extension-from-file
title: How to install VSCode extension from file
authors: [cristi]
tags: [extension, from file, install, vscode, vscodium]
date: 2020-11-09T19:17
---

# How to install VSCode extension from file

Sometimes VSCodium search functionality for extensions doesn't work, or doesn't return the desired results. So to fix this I had to look for an alternative:

Download the extension from [VSCode marketplace](https://marketplace.visualstudio.com/), it'll be a `.vsix` file, then do like the image below.

![](https://www.hreniuc.dev/wp-content/uploads/2020/11/vs_code_extension_2.jpg)

![](https://www.hreniuc.dev/wp-content/uploads/2020/11/vscode_extension_1.jpg)


Source: https://github.com/microsoft/vscode/issues/108147#issuecomment-704723949