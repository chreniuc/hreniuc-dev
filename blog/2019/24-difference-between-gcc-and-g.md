---
slug: difference-between-gcc-and-g
title: Difference between gcc and g++
authors: [sebi]
tags: [gcc, g++]
date: 2019-07-25T19:49
---

# Difference between gcc and g++

`gcc` and `g++` are both compiler drivers. First one is used for C applications while the last one for C++.
Even so, they both can be used interchangeably (e.g. use gcc for C++ or g++ for C). This is because in the end both will end up using the same tools (at least for `.cpp` files) for running the desired action, only the default flags used will differ. This can be analysed by appending the flag `-v` to both drivers, which will make them output the things done in the background.

The table below shows the notable differences.

| Driver command  | Compiler flags  | Assembler flags | Linker flags |
| ------------ | ------------ | ------------ | ------------ |
| `gcc main.cpp -o test -v`  | - | - | `-lgcc --as-needed -lgcc_s --no-as-needed -lc -lgcc --as-needed -lgcc_s --no-as-needed` |
| `g++ main.cpp -o test -v`  | - | - | `-lstdc++ -lm -lgcc_s -lgcc -lc -lgcc_s -lgcc` _Also the option -shared-libgcc was automatically given as a gcc option_|
| `gcc main.c -o test -v`  | cc1 | - | `-lgcc --as-needed -lgcc_s --no-as-needed -lc -lgcc --as-needed -lgcc_s --no-as-needed` |
| `g++ main.c -o test -v`  | cc1plus -D_GNU_SOURCE | - | `-lstdc++ -lm -lgcc_s -lgcc -lc -lgcc_s -lgcc` _Also the option -shared-libgcc was automatically given as a gcc option_|

Basically, `g++` is equivalent to `gcc -shared-libgcc -lstdc++ -lm` and for c files, g++ will also use the compiler `cc1plus` while gcc will use `cc1`.



_Later edit: If you're interested in the -\-as-needed flag you can checkout [this](https://stackoverflow.com/a/6412134/8946639) stackoverflow link_