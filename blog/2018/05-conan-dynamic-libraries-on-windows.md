---
slug: conan-dynamic-libraries-on-windows
title: Conan, dynamic libraries on windows
authors: [cristi]
tags: [c++ package manager, conan, dynamic library, windows]
date: 2018-03-31T21:04
---

# Conan, dynamic libraries on windows

In your application you have to copy the dynamic libraries to the user space from the local cache. To do that you have to have the **import** method in the **conanfile.py** from your application. You will have to paste this code in the **conanfile.py** from your project:

```py
def import(self):
  self.copy(*.dll, bin, bin)
  self.copy(*.dylib, bin, bin)
```

Here is the official documentation of the method: [http://docs.conan.io/en/latest/reference/conanfile/methods.html#imports](http://docs.conan.io/en/latest/reference/conanfile/methods.html#imports)