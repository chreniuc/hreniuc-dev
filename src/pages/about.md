---
title: About
---

# Hreniuc Cristian Alexandru...

Funny: [Remember: reinterpret_cast is fragile. Sneeze and you get a segfault.](http://www.cplusplus.com/forum/general/21518/#msg112504)


>The First Amendment to the C++ Standard: The committee shall make no rule that prevents programmers from shooting themselves in the foot.
[Bjarne Stroustrup Quotes](https://www.stroustrup.com/quotes.html)