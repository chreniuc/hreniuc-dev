---
slug: pre-defined-compiler-macros
title: Pre-defined Compiler Macros
authors: [cristi]
tags: [c++, gcc, macros, pre-defined macros]
date: 2017-08-28T13:03
---

# Pre-defined Compiler Macros

**On Windows**

|  | Identification |  | `_WIN32  ` |  | Defined for both 32-bit and 64-bit environments [1](http://msdn.microsoft.com/en-us/library/ff540443.aspx) |  |
|---|----------------|---|--------------|---|----------------------------------------------------------------------------------------------------------------|---|

**Linux**

|  | Identification |  | `__linux__   ` |  | [1](http://www.faqs.org/docs/Linux-HOWTO/GCC-HOWTO.html#INDEX.25) |  |
|---|----------------|---|-------------------|---|-----------------------------------------------------------------------|---|

[https://sourceforge.net/p/predef/wiki/OperatingSystems/](https://sourceforge.net/p/predef/wiki/OperatingSystems/)