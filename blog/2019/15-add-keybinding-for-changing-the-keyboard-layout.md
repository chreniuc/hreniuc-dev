---
slug: add-keybinding-for-changing-the-keyboard-layout
title: Add keybinding for changing the keyboard layout
authors: [cristi]
tags: [archlinux, debian, keyboadr, keyboard layout, linux, romanian, switch keyboad layout, us]
date: 2019-06-09T19:15
---

# Add keybinding for changing the keyboard layout

This will add a keybinding (`Alt+Shift`) to switch the keyboard layout between `us` are `ro`.

Run:

```bash
echo 'Section "InputClass"
Identifier "Keyboard Defaults"
MatchIsKeyboard "yes"
Option "XkbLayout" "us, ro(std)"
Option "XkbOptions" "grp:alt_shift_toggle, grp_led:scroll"
EndSection' >> /etc/X11/xorg.conf.d/10-keyboard.conf
```

Reboot or restart xserver.

Works on every distribution. First check if `/etc/X11/xorg.conf.d/10-keyboard.conf` does exits, if it exists write to another file: eg: `/etc/X11/xorg.conf.d/20-keyboard.conf`.

On `Debian` with `lxde`, the path for `xorg.conf.d` is `/usr/share/X11/xorg.conf.d`.

Press `Alt+Shift` to switch between `us` and `ro`.

If you want other key, for ex: `win` : `Option "XkbOptions" "grp:win_shift_toggle, grp_led:scroll"`.
