---
slug: how-to-use-cpupower-cpu-governors
title: How to use cpupower, cpu governors.
authors: [cristi]
tags: [boost cpu, change frequency, cpu, cpugovernor, cpupower, governor]
date: 2019-03-28T19:11
---

# How to use cpupower, cpu governors.

Boost your CPU performance by changing the CPU governor.

List all options available of the `cpupower`:

```bash
$ cpupower

# Example output:
Usage:  cpupower [-d|--debug] [-c|--cpu cpulist ] <command> [<args>]
Supported commands are:
        frequency-info
        frequency-set
        idle-info
        idle-set
        set
        info
        monitor
        help

Not all commands can make use of the -c cpulist option.

Use 'cpupower help <command>' for getting help for above commands.
```

See the curent governor:

```bash
# For one CPU(Core)
$ cpupower frequency-info
# Example output
analyzing CPU 0:
  driver: intel_pstate
  CPUs which run at the same hardware frequency: 0
  CPUs which need to have their frequency coordinated by software: 0
  maximum transition latency:  Cannot determine or is not supported.
  hardware limits: 800 MHz - 4.50 GHz
  available cpufreq governors: performance powersave
  current policy: frequency should be within 800 MHz and 4.50 GHz.
                  The governor "powersave" may decide which speed to use
                  within this range.
  current CPU frequency: 1.87 GHz (asserted by call to hardware)
  boost state support:
    Supported: yes
    Active: yes


# For all cores
$ cpupower -c all frequency-info
```

Get table with all frequencies for each core:

```bash
$ cpupower monitor

# Example output:

    |Nehalem                    || Mperf              || Idle_Stats                                                   
CPU | C3   | C6   | PC3  | PC6  || C0   | Cx   | Freq || POLL | C1-S | C1E- | C3-S | C6-S | C7s- | C8-S | C9-S | C10- 
   0|  4.89|  7.70|  0.00|  0.00|| 23.03| 76.97|   915||  0.03| 17.99| 40.21|  8.74| 11.80|  0.00|  0.00|  0.00|  0.00
   6|  4.89|  7.70|  0.00|  0.00|| 11.26| 88.74|   929||  0.01|  2.11| 17.31| 10.02| 59.94|  0.00|  0.35|  0.00|  0.00
   1|  5.58|  6.59|  0.00|  0.00|| 25.42| 74.58|   916||  0.04| 17.29| 36.64| 10.60| 11.77|  0.00|  0.00|  0.00|  0.00
   7|  5.58|  6.59|  0.00|  0.00|| 14.18| 85.82|   949||  0.01|  2.69| 18.36| 11.53| 53.34|  0.00|  0.91|  0.00|  0.00
   2|  5.11|  7.93|  0.00|  0.00|| 23.03| 76.97|   919||  0.05| 18.25| 37.32|  9.48| 13.66|  0.00|  0.00|  0.00|  0.00
   8|  5.11|  7.93|  0.00|  0.00|| 12.20| 87.80|   914||  0.01|  2.88| 19.60| 10.31| 55.49|  0.00|  0.56|  0.00|  0.00
   3|  5.70|  8.69|  0.00|  0.00|| 24.53| 75.47|   921||  0.04| 16.46| 34.43| 10.49| 15.76|  0.00|  0.00|  0.00|  0.00
   9|  5.70|  8.69|  0.00|  0.00|| 13.63| 86.37|   913||  0.00|  5.32| 22.42|  9.91| 50.01|  0.00|  0.00|  0.00|  0.00
   4|  6.48|  9.17|  0.00|  0.00|| 24.33| 75.67|   922||  0.05| 17.97| 35.34| 10.25| 13.92|  0.00|  0.00|  0.00|  0.00
  10|  6.48|  9.17|  0.00|  0.00|| 10.87| 89.13|   925||  0.00|  1.55| 15.92|  8.78| 62.75|  0.00|  1.00|  0.13|  0.00
   5|  5.84|  9.83|  0.00|  0.00|| 23.58| 76.42|   915||  0.03| 16.77| 37.21| 10.00| 14.15|  0.00|  0.00|  0.00|  0.00
  11|  5.84|  9.83|  0.00|  0.00|| 10.60| 89.40|   916||  0.00|  2.14| 16.65|  8.55| 62.33|  0.00|  0.71|  0.00|  0.00
```

Set a specific governor:

*Not you can one set a supported governor, run `cpupower frequency-info` and check this line: `available cpufreq governors: performance powersave`*

```bash
$ cpupower frequency-set -g performance
```

Check if the governor was successfully changed:

```bash
$ cpupower -c all frequency-info

# Example output:
analyzing CPU 0:
  driver: intel_pstate
  CPUs which run at the same hardware frequency: 0
  CPUs which need to have their frequency coordinated by software: 0
  maximum transition latency:  Cannot determine or is not supported.
  hardware limits: 800 MHz - 4.50 GHz
  available cpufreq governors: performance powersave
  current policy: frequency should be within 800 MHz and 4.50 GHz.
                  The governor "performance" may decide which speed to use
                  within this range.
  current CPU frequency: 4.20 GHz (asserted by call to hardware)
  boost state support:
    Supported: yes
    Active: yes
```

