---
slug: ssh-tunneling
title: SSH tunneling
authors: [cristi]
tags: [port forwarding, ssh, tunneling, linux]
date: 2019-02-05T19:02
---

# SSH tunneling

Let's say we have this diagram of servers:

**A:9104** -> **B** -> **C:9104**

Connect from server A to server C via server B:

```bash
# We are here on server A and we run the following command:
ssh -N -f -L \*:9104:C:9104 user@B
# -N do not run any command on remote server
# -f run in background
# -L local forward
# \* - the 9014 port will be publically visible from outside of server A
```

Now to access `C:9104`, you will be able to do it from A like this `A:9104`.

Why would you want to do this? When you can't access directly the server C from server A, but you can access it from server B. And from server A you can access B. Voila!
