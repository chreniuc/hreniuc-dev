---
slug: mysql-useful-queries
title: Mysql useful queries
authors: [cristi]
tags: [btree, global status, global variables, indexes, mariadb, rename table, sql]
date: 2019-03-28T19:10
---

# Mysql useful queries

**Show indexes for a specific table:**

```sql
SHOW INDEXES FROM TABLE_NAME;
```

*Note: INDEX_TYPE ='HASH' is slower than INDEX_TYPE ='BTREE'*

**Show indexes for tables from a specific database or all databases:**

```sql
SELECT *
FROM INFORMATION_SCHEMA.STATISTICS
[WHERE TABLE_SCHEMA = 'database_name'];
```

**Add new index for a specific table:**

```sql
ALTER TABLE `table_name` ADD INDEX `index_name` USING BTREE(`column1`, `column2`);
```


**Rename table:**

```sql
ALTER TABLE current_table_name RENAME new_table_name;
```

**Show global system variables:**

```sql
SHOW GLOBAL variables;
```

**Set global variable:**

```sql
SET GLOBAL long_query_time = 20;
```

*This will have effect only if the field is dynamic, check the [documentaion](https://mariadb.com/kb/en/library/server-system-variables/#long_query_time) for each variable.*

**Show global status:**

```sql
SHOW GLOBAL status;
```

**Get size total size of all MEMORY tables:**

```sql
SELECT (data_length+index_length)/(1024*1024*1024) table_size,
       TABLE_NAME,
       TABLE_SCHEMA
FROM information_schema.tables
WHERE ENGINE LIKE 'MEMORY'
ORDER BY table_size DESC;
```