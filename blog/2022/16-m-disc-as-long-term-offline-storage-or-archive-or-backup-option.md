---
slug: m-disc-as-long-term-offline-storage-or-archive-or-backup-option
title: M-DISC as long term offline storage or archive or backup option
authors: [cristi]
tags: [m-disc, storage, archive, backup, offline, long term]
date: 2022-09-26T16:56
---

# M-DISC as long term offline storage or archive or backup option

I was thinking on how can you archive your data and store it without loosing it. Maybe first option that you are thinking is HDD or SSD, but those do not last very long(3-5 years).

So I tried to find an alternative, to store my backups and I found out about [M-DISC](https://www.mdisc.com/faq.html).

They say that it can store data for 1000 years. I also found some durability tests for this technology:

 - [English](http://www.microscopy-uk.org.uk/mag/indexmag.html?http://www.microscopy-uk.org.uk/mag/artsep16/mol-mdisc-review.html)
 - [Romanian](https://www.ghiduldslr.ro/verbatim-bd-xl-m-disc/)

A summary of those:

 - write the data to the disc
 - Put the disc in water and keep it there for a few hours
 - Freeze it for 24/48 hours
 - Leave it outside for 24 hours in the sun/wind
 - Scratch it
 - Try to read from it -> **Success**

You will need optical disks that use M-DISC technology and also an optical writer that supports M-DISC technology.