---
slug: web-interface-for-transmission-remote-torrent-client
title: Web interface for transmission - remote torrent client
authors: [cristi]
tags: [debian, linux, remote torrent, torrent, transmission]
date: 2019-09-22T19:30
---

# Web interface for transmission - remote torrent client

[Source](https://linuxconfig.org/how-to-set-up-transmission-daemon-on-a-raspberry-pi-and-control-it-via-web-interface)

If you need to connect remote to a PC and download something via torrent you need to do these steps:


Install `transmission-daemon`:

```bash
apt-get install transmission-daemon
```

This will install and start the service: `transmission-daemon`.

If you want to check it's status you can run:

```bash
systemctl status transmission-daemon
```

This will start a service and a web interface to acces the torrent client. This client will be at: `ip:9091/transmission`.

If you open that url, it will ask a user and password, by default these are:`transmission:transmission`.

If you want to change something, ex: the username, password, port; you can edit the settings file: `/etc/transmission-daemon/settings.json`. But before stop the deamon:

```bash
systemctl stop transmission-daemon
# After edit, start the daemon
systemctl start transmission-daemon
```

By default the `download_dir` is set to `/var/lib/transmission-daemon/downloads` you can change it to any value.

For username and password for the web interface: `rpc-username` and `rpc-password`(put it in plain, it will be hashed automatically after daemon starts).

If you want to access this interface from remote, you can do a tunnel via ssh:

```bash
ssh -L 127.0.0.1:9091:127.0.0.1:9091 user@pc
```