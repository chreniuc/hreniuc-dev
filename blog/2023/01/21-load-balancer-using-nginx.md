---
slug: load-balancer-using-nginx
title: "Load balancer using nginx"
authors: [cristi]
tags: [docker, docker-compose, nginx, load balancer]
date: 2023-01-22T20:01
---

# Load balancer using nginx

I needed this when I switched from kubernetes to docker compose(Kubernetes was too much to maintain and I only had one small project).

To load balance I used the following config for nginx:

```bash
upstream pool-of-services {
  # Resolved by docker dns, so they must exist at nginx startup, othewise this fails
  server container_1:6070;
  server container_2:6070;
}

server {
    # Listen to port 443 on both IPv4 and IPv6.
    listen 443 ssl;
    listen [::]:443 ssl;

    location / {
      proxy_set_header   Host             $host;
      proxy_set_header   X-Real-IP        $remote_addr;
      proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
      proxy_pass http://pool-of-services;
    }
}
```

[Source](https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/)