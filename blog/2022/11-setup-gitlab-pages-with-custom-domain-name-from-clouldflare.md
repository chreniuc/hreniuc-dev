---
slug: setup-gitlab-pages-with-custom-domain-name-from-clouldflare
title: Setup Gitlab Pages with custom domain name from Clouldflare
authors: [cristi]
tags: [docusaurus, personal-website, gitlab, gitlab pages]
date: 2022-09-21T20:41
---

# Setup Gitlab Pages with custom domain name from Clouldflare

After I switched to [docusaurus](https://docusaurus.io/) I also wanted to make the deployment easier and I had two options:

 - selfhost the website(as I did previously with wordpress)
 - host it via gitlab pages

I went with the second option, due to the fac that for the first option I had to use ssh credentials to connect to my hosting provider and copy the files there. I wanted to avoid doing that.

So I choose [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) which is the same as Github Pages. The advantage of this is that it auto updates the website with just a simple CI job.

So to do this I had to follow these steps:

 - I went to the [Gitlab pages docs](https://docs.gitlab.com/ee/user/project/pages/)
 - Create a repository for your website
 - Then go to `Settings` > `Pages`
 - It will open a page where you need to configure the `gitlabci.yml` for your website to be build and copied to a `public` folder, [this](https://gitlab.com/chreniuc/hreniuc-dev/-/blob/master/.gitlab-ci.yml) is the generated file for my website
 - The pipeline will be triggered after you complete all steps and if it passes it will generate a page for it on the gitlab domain, eg: `chreniuc.gitlab.io`
 - I wanted to use my custom domain name, so to do this I went back to `Settings` > `Pages` and there was an option to add a domain, like it's stated in [this documentation](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/), the pictures from that page are outdated.
 - My domain is managed from CLouldflare, so I had to do the following:
   - Add the name of the domain in that section from gitlab
   - I checked to use Let's encrypt for SSL certs
   - In Clouldflare I had to add an `A` record for `@`(hreniuc.dev) to point to `35.185.44.232`, not to `chreniuc.gitlab.io.` as they state in the doc, Clouldflare didn't allow me to use a DNS there, only IP
   - In Clouldflare I had to add an `TXT` record for `_gitlab-pages-verification-code` with a value to verify it, this will be used for validation that you own the domain
   - I also had to add a rule in Clouldflare to redirect from `www.hreniuc.dev` to `hreniuc.dev`, because I don;t know why, but when I was trying to access `www.hreniuc.dev` I ended up getting 401 Not authorized, [like here](https://webmasters.stackexchange.com/questions/137455/www-domain-not-working-with-gitlab-pages). The rule looked like this: `www.hreniuc.dev/*` > `301 Permanent Redirect` > `https://hreniuc.dev/$1`
   - I saved the domain in the Gitlab Pages section and clicked on an icon to retry verifying the domain and it worked


Now, everytime I pushed changes to the default branch it auto generated a pipeline and updated the website.