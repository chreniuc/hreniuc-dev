---
slug: updating-rke2-cluster-nodes-config
title: Updating rke2 cluster nodes config
authors: [cristi]
tags: [kubernetes, rke, rke2, update,config ]
date: 2023-01-05T16:01
---

# Updating rke2 cluster nodes config

**Change the configs for our nodes on each server**

```bash
# Contabo
ssh -i ~/.ssh/ansible_noob_id_rsa ansible_noob@SERVER-IP-1 # Master
ssh -i ~/.ssh/ansible_noob_id_rsa ansible_noob@SERVER-IP-2


sudo su -
# Sometimes you might need to only change on the server config
nano /etc/rancher/rke2/config.yaml

# For server
systemctl restart rke2-server 

# For agent
systemctl restart rke2-agent

# Check the logs
journalctl -u rke2-server -f
journalctl -u rke2-agent -f
```