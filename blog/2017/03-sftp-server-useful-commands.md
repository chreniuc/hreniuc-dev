---
slug: sftp-server-useful-commands
title: sftp server useful commands
authors: [cristi]
tags: [begginer, commands, linux, sftp]
date: 2017-08-15T13:02
---

# sftp server useful commands

**Connect to server:**

```bash
sftp -P [port no] user@dns_or_ip
```

**Commands for the remote server:**

```bash
pwd #print remote working directory
cd #open remote directory
ls #list remote files
```

**Commands for the remote server(add a 'l' before each command):**

```bash
lpwd # Print local working directory
lcd # Open local directory
lls # List local files
```

**Transfer remote file to local:**

```bash
get remote_file 
# Rename the file that you want to get from remote
get remote_file new_name
# Get a directory(recursive)
get -r remote_directory
# Keep the same permissions to the files
get -Pr remote_directory
```

**Send file from local to remote(Same as before, use 'put' instead of 'get'):**

```bash
put local_file 
# Rename the file that you want to get from local
put local_file new_name
# Get a directory(recursive)
put -r remote_directory
# Keep the same permissions to the files
put -Pr remote_directory
```

**Exit:**

```bash
exit
```