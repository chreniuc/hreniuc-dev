---
slug: ucspi-tcp-package-tips
title: UCSPI-TCP package tips
authors: [cristi]
tags: [bash, linux, mock tcpserver, multiple tcp connections, tcpserver, terminal]
date: 2019-08-02T19:25
---

# UCSPI-TCP package tips

Install `ucspi-tcp`:

```bash
apt-get install ucspi-tcp
```

This pacakge contains this list of [tools](https://manpages.debian.org/testing/ucspi-tcp/index.html)

Create tcp server from bash using `tcpserver` command from `ucspi-tcp`:

```bash
# script.sh content
#!/bin/bash
# read MESSAGE
echo "message: $MESSAGE"
sleep 2
echo " message 2"
sleep 3
echo " message 3"
sleep 2
# ---- end ------- #

# Run tcpserver:
tcpserver -v -H -R IP PORT script.sh

# Open a connection:
echo "" | nc IP PORT
```

This tcp server can open multiple connections in parallel. Verry usefull when you want to have a mock tcpserver.

Fixed issues:

1. The `read` command doesn't work if I add it in the script(I had a client written in C++). But it did work when my client was `nc`....
 
The fix was to add `\n` at the end of the string that I was sending from that client. To reproduce this, you can run a client like this: `echo -n "sa" | nc IP PORT` and the script should contain a `read` instruction. You will notice that the server will stop at that `read` instruction.
