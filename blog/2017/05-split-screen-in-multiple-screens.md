---
slug: split-screen-in-multiple-screens
title: Split screen in multiple screens
authors: [cristi]
tags: [ fakexrandr, linux, split monitor, split screen, ubuntu, ultrawide]
date: 2017-10-07T13:04
---

# Split screen in multiple screens

This is a tutorial on how you can split a screen in multiple screens on linux. I needed this because i got an ultrawide monitor.

For this we will use: fakexrandr.

We will begin by cloning the project: git clone https://github.com/phillipberndt/fakexrandr.git

Then we will only have to install it using :

```bash
make
make install
```

**Note:** If we get an error like: "Unable to locate package pkg-config" You will have to install pkg-config:

```bash
apt-get install pkg-config
```

You will also need to install :

```bash
apt-get install libxrandr-dev
```

You might get an error: `libXrandr.c:18:37: fatal error: X11/extensions/Xinerama.h: No such file or directory`

```bash
apt-get install libx11-dev libxinerama-dev
```

Now all you have to do is to run :

```bash
fakexrandr-manage gui
```

Note: To use the graphical interface you will have to install pygobject:

```bash
apt install python-gi
# or
apt install python-gi-dev
```

After all these steps i got the same problem as this user [here](https://github.com/phillipberndt/fakexrandr/issues/40)  but i managed to do it via CLI. Below you can see how i did it.

[phillipberndt](https://github.com/phillipberndt) explains it very clearly in his response:

    Hm, simply run fakexrandr-manage.py show-available, copy the output, and replace SPLITS="N" with SPLITS="whatever", where for whatever you subsitute your splits. From the command's output:

        It starts by one of the letters H, V or N, describing the
        kind of split. H means horizontal, V vertical and N no split. Separated by a
        space follows the pixel position of the split. Again separated by a space
        follow the two sub-configurations of the left/right or top/bottom halves. Any
        additional white-space besides a single space is optional any only serves
        better readibility. dump-config indents sub-configurations to this end.
        If SPLITS equals N, a configuration is discarded upon saving it.

    That is, to have one split (step 1 in the screencast above), insert e.g. V 840 N N to split at 840px, to have the right half split further horizontally (as in step 2), insert V 840 N H 630 N N to split at 630px. This becomes more clear if you insert more whitespace:
    ```
    V              # Split vertically
    840            # at 840px
        N          # Do not split the left half further
        H          # Split the right half horizontally
        630        # At 630px
            N      # Do not split the top half further
            N      # Do not split the lower half further
    ```

Let's say you want to split the monitor in two, horizontally. Initially fakexrandr-manage show-available will give you the output:

```bash
NAME="HDMI-A-0" # output name
EDID=00ffffffffffff001e6d # a long hex string
WIDTH=2560
HEIGHT=1080
SPLITS="N"
```

Copy all that output and change only the SPLITS field to : SPLITS="V 1280 N N" . This will split your monitor horizontally in two equal screens(if your resolution is 2560) .(Those two "N N" mean that you don't want to split those two halves that resulted from the first split) Put everything into a file file_with_config, after that run fakexrandr-manage set-config < file_with_config

To see if the config is being used run: fakexrandr-manage dump-config this will output the current config that is being used.

To make it work you have to restart the desktop manager or reboot. All those things were tested on Lubuntu 17.04 and it worked.
