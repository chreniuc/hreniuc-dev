---
slug: run-gui-apps-with-docker
title: Run GUI apps with docker
authors: [cristi]
tags: [docker, gui apps in docker, linux, x11 forwarding]
date: 2018-06-18T21:06
---

# Run GUI apps with docker

Debian host - ubuntu docker:

```bash
# Allow x11 forwarding
xhosts +

docker run --rm=true --name="container_name" -e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix -it image_name /bin/bash
# This is the most important part from the command:
# -e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix
```

# Run GUI apps on docker without xserver installed on host

We have three layers: **A** -> My desktop **PC****B** -> Host server(runs docker) **C** -> docker container that runs on **B**How to connect to **C** and run a GUI app from **A**? Without having installed xserver on **B** (no `-e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix` options given to the docker run command). I did it this way:

1. Create a docker image with ssh running on it: [Here](https://docs.docker.com/engine/examples/running_ssh_service/#run-a-test_sshd-container)

```dockerfile
  FROM ubuntu:16.04
  
  RUN apt-get update && apt-get install -y openssh-server
  RUN mkdir /var/run/sshd
  RUN echo 'root:screencast' | chpasswd
  RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
  
  # SSH login fix. Otherwise user is kicked off after login
  RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
  
  ENV NOTVISIBLE in users profile
  RUN echo export VISIBLE=now >> /etc/profile
  
  EXPOSE 22
  CMD [/usr/sbin/sshd, -D]
```

2. Build the image: docker build -t ubuntu_sshd .
3. Run the container: docker run -d -P --name test_sshd ubuntu_sshd
4. Get port of the docker container: docker port test_sshd 22
5. Now connect to it using ssh:

  ```bash
  ssh root@ip -p port
  # The password is ``screencast``.
  root@f38c87f2a42d:/#
  ```
6. Enable X11Forwarding : Add `X11Forwarding yes` to `/etc/ssh/sshd_config`
7. Disable X11UseLocalhost : Add X11UseLocalhost no to /etc/ssh/sshd_config
8. Reload ssh config: /etc/init.d/ssh reload
9. Exit from the ssh session
10. All those commands were made from the **B** machine(from 5 - 8 were run on the docker container)
11. Now go to your desktop machine **A** and connect via ssh on the docker machine(also enable x forwarding on your destktop PC), using the ip of **B** and the port you got from the command at step **4**:

```bash
  # Allow xserver forward(allow clients to connect to your x server)
  $ xhost +
  # Connect via ssh and forward the xserver `-X`
  $ ssh -X root@B_host_ip -p docker_port
  # The password is ``screencast``.
  root@f38c87f2a42d:/# apt-get install x11-apps -y
  root@f38c87f2a42d:/# xeyes
  # Magic happens!!!
```

**Debian:**

```dockerfile
# Debian with sshd
FROM debian
RUN apt-get update

RUN apt-get install -y openssh-server

RUN echo 'root:password' | chpasswd

RUN mkdir -p /var/run/sshd && sed -i s/UsePrivilegeSeparation.*//g /etc/ssh/sshd_config \
  && sed -i 's/PermitRootLogin without-password//' /etc/ssh/sshd_config \
  && touch /root/.Xauthority \
  && true

RUN sed -i 's/PermitRootLogin .*/PermitRootLogin yes/' /etc/ssh/sshd_config

RUN echo X11UseLocalhost no >> /etc/ssh/sshd_config
RUN echo PermitRootLogin yes >> /etc/ssh/sshd_config
RUN echo UsePrivilegeSeparation no >> /etc/ssh/sshd_config

EXPOSE 22

CMD [/usr/sbin/sshd, -D]
```