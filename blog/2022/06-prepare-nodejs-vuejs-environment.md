---
slug: prepare-nodejs-vuejs-environment
title: Prepare nodejs, vuejs environment
authors: [cristi]
tags: [nodejs, vuejs, node, web]
date: 2022-05-14T19:06
---

# Prepare nodejs, vuejs environment

**Installl nvm**

```bash
# Check latest release: https://github.com/nvm-sh/nvm/releases/latest
# This commands also adds the paths to your ~/.bashrc
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
source ~/.bashrc
```

**Install latested node version and use it**

```bash
nvm install --lts
nvm use --lts
# Or
nvm use 16.15.0
```

Install `yarn` and upgrade npm:

```bash
npm install --global yarn

npm install -g npm@8.10.0
```

Install [RESTer](https://addons.mozilla.org/en/firefox/addon/rester/) extension.