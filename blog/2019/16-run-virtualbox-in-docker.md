---
slug: run-virtualbox-in-docker
title: Run virtualbox in docker
authors: [cristi]
tags: [docker, linux, virtualbox]
date: 2019-06-10T19:16
---

# Run virtualbox in docker

```bash
# What does --device /dev/vboxdrv:/dev/vboxdrv do?
# Does it write to the host system?

docker run --rm=true --name="container_name" \
  --privileged=true --device /dev/vboxdrv:/dev/vboxdrv \
  -e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix \
  -it \
  mpfmedical/virtualbo
# In container:
$ virtualbox
```