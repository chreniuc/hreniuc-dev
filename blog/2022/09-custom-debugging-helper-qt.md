---
slug: adding-custom-debugging-helpers-in-qtcreator
title: Adding Custom Debugging Helpers in QtCreator
authors: [cristi]
tags: [gdb, python, Qtcreator, c++]
date: 2022-06-29T13:09
---

# Adding Custom Debugging Helpers in QtCreator

Source: https://doc.qt.io/qtcreator/creator-debugging-helpers.html#adding-custom-debugging-helpers

```bash
find ~ -name personaltypes.py

/home/cristi/Qt/Tools/QtCreator/share/qtcreator/debugger/personaltypes.py
```

Add there the following contents:

```py
def qdump__ClassType1(d, value):
    m_member1 = value["m_member1"].integer()
    m_member2 = value["m_member2"].integer()
    miliseconds = int(m_member1 * 1000 / m_member2)
    d.putNumChild(0)
    d.putValue(str(miliseconds) + str("ms (")+ str(m_member1) + str("m1, ") + str(m_member2) + str("m2)"))

def qdump__ClassType2(d, value):
    position = value["m_position"]
    d.putNumChild(0)
    qdump__FramesDuration(d, position)
```

The `ClassType2` will be displayed like this: `3642331ms (160626834m1, 44100m2)`.

For more examples, check: `/home/cristi/Qt/Tools/QtCreator/share/qtcreator/debugger/stdtypes.py`.