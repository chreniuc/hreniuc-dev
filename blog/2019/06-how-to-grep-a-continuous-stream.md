---
slug: how-to-grep-a-continuous-stream
title: How to grep a continuous stream?
authors: [cristi]
tags: [continuous, flush, grep, linux]
date: 2019-02-14T19:06
---

# How to grep a continuous stream?


```bash
tail -f out | grep --line-buffered "text"
```

Or:

```bash
tail -f out | stdbuf -i0 -o0 -e0 grep "text"
```