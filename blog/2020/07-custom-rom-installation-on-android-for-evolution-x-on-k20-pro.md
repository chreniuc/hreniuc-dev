---
slug: custom-rom-installation-on-android-for-evolution-x-on-k20-pro
title: Custom ROM installation on android for Evolution X on K20 pro
authors: [cristi]
tags: [android, flash, root, rom]
date: 2020-06-12T19:07
---

# Custom ROM installation on android for Evolution X on K20 pro

#### First Time Install / Clean Flash

1. Reboot to Recovery
2. Download the proper ZIP for your device
3. Wipe Data/Cache/System when coming from non-AOSP ROMs
4. Flash MIUI 11 vendor/firmware for your device variant
5. Flash the ROM
6. Reboot to System
7. Reboot to Recovery
8. Flash Magisk (Optional)
9. Reboot to System and **#KeepEvolving**

#### Update / Dirty Flash

1. Reboot to Recovery
2. Download the proper ZIP for your device
3. Flash the ROM
4. Reboot to System and #KeepEvolving

Install tools:

```bash
 sudo apt-get install android-tools-adb android-tools-fastboot
```

Use sideload to flash:

```bash
sudo adb devices

sudo adb reboot recovery
```

Wait for recovery to load, you should end up in twrp menu. Go to `Advanced -> ADB Sideload` and 

```
 adb sideload EvolutionX_4.20.1_raphael-10.0-20200417-0747-OFFICIAL.zip

# optional, update Magisk
adb sideload Magisk-v20.4.zip
```

Source: https://forum.xda-developers.com/k20-pro/development/rom-evolution-x-3-3-t4002669

To do:

 - https://forum.xda-developers.com/showpost.php?p=82758491&postcount=5864
  - TWRP : https://forum.xda-developers.com/k20-pro/development/recovery-unofficial-twrp-xiaomi-redmi-t3944363