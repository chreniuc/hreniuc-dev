---
slug: default-build-folder-in-qtcreator
title: Default build folder in QtCreator
authors: [cristi]
tags: [build folder, custom path, Qtcreator, c++]
date: 2019-03-04T19:07
---

# Default build folder in QtCreator


Create a build folder for each project called `project_name`, which will contain multiple subfolders for each `build_type`.

Go to `Tools` -> `Options` -> `Build & Run` -> `Default build directory`:

```bash
/path/to/builds/%{JS: Util.asciify("%{CurrentProject:Name}/build-%{CurrentProject:Name}-%{CurrentKit:FileSystemName}-%{CurrentBuild:Name}")}
```

This will result in a a tree of folders:
 - `builds`
   - `project_name`
     - `build-project_name-Kit_name-Build_type`
   - `project_name2`
    - `build-project_name2-Kit_name-Build_type`

Or we can set it to :

```bash
/path/to/builds/%{JS: Util.asciify("%{CurrentProject:Name}/%{CurrentBuild:Name}")}
```

This will result in a a tree of folders:
 - `builds`
   - `project_name`
     - `Release`
     - `Debug`
   - `project_name2`
    - `build_type`