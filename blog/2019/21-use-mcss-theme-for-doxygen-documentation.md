---
slug: use-mcss-theme-for-doxygen-documentation
title: Use mcss theme for doxygen documentation
authors: [cristi]
tags: [config folder, linux, vscodium]
date: 2019-07-15T19:21
---

# Use mcss theme for doxygen documentation

A tutorial can be found [here](https://mcss.mosra.cz/documentation/doxygen/).

___

First you need to install/build the latest version of doxygen, [here](http://www.linuxfromscratch.org/blfs/view/svn/general/doxygen.html) you have a tutorial.

```bash
yum install flex bison

wget http://doxygen.nl/files/doxygen-1.8.15.src.tar.gz
tar -xf doxygen-1.8.15.src.tar.gz
mkdir -v build &&
cd       build &&

cmake -G "Unix Makefiles"         \
      -DCMAKE_BUILD_TYPE=Release  \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -Wno-dev .. &&

make -j16

make install
```

*Notice: You will also need to install flex and bison, before building the doxygen binary.*

Generate the documentation:

```bash
git clone git://github.com/mosra/m.css
cd m.css/documentation

# You may need sudo here
pip3 install jinja2 Pygments --user
```

Create a `Doxyfile-mcss` alongside your doxygen.cfg file with this content:

```doxygen
@INCLUDE                = doxygen.cfg
GENERATE_HTML           = NO
GENERATE_XML            = YES
XML_PROGRAMLISTING      = NO
```

Generate the documentation:

```bash
./doxygen.py path/to/your/Doxyfile-mcss
```

Then go in the output folder(the one set in the `doxygen.cfg`) and open index.html.

*Note: Check the unsuported features [here](https://mcss.mosra.cz/documentation/doxygen/#intentionally-unsupported-features). Ex: Class hierarchy graphs are ignored..*