---
slug: activate-mariadb-general-logs-at-runtime-log-all-queries
title: "Activate mariadb general logs at runtime - log all queries"
authors: [cristi]
tags: [mariadb, logs, query, sql, database]
date: 2023-01-18T19:01
---

# Activate mariadb general logs at runtime - log all queries

You can do this using these queries:

```sql
SET GLOBAL general_log_file='mariadb.log';
SET GLOBAL general_log_file='/var/log/mariadb/query.log';
SHOW VARIABLES LIKE '%general_log_file%';
SET GLOBAL general_log=1;
```

And then access your logs like this:

```bash
tail -f /var/log/mariadb/query.log
```

Resources:
 - [show-variables](https://mariadb.com/kb/en/show-variables/)
 - [general-query-log](https://mariadb.com/kb/en/general-query-log/)