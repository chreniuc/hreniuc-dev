---
slug: steps-to-root-and-flash-aicp-on-leeco-le-pro3
title: Steps to Root and Flash AICP on LeEco Le Pro3
authors: [cristi]
tags: [android, how to, flash,root, AICP]
date: 2019-01-06T19:01
---

# Steps to Root and Flash AICP on LeEco Le Pro3


The main source of inspiration were this [thread](https://forum.xda-developers.com/le-pro3/development/rom-aicp-13-0-pro3-aka-zl1-x727-x720-t3698058) and this [thread](https://forum.xda-developers.com/le-pro3/leeco-le-pro-3-elite-model-x722-guides-development--and-mods/guide-unlock-bootloader-twrp-root-t3728582).

The first steps are to install the specific tools on linux to be able to flash a new OS on the phone. On my PC I have Arch Linux, I had to install `android-tools` : `sudo pacman -S android-tools`

### **Check if the phone is rooted**

Before trying to root your phone, check if it\'s not already rooted(The Chinese phones come already rooted).

To do this you need to follow this steps:

- activate `USB debugging` in your phone(Developer options).
- Connect the phone via USB(Choose `File transfer` when the USB popup appears on the phone)
- Check if the device is visible in `adb`: `adb devices`, it should show a device, not an empty list.
- Run command `sudo adb reboot bootloader`, this will reboot the phone in bootloader mode. Another method is to press and hold `volume up` and `power` buttons. Source: [here](https://www.recovery-mode.com/android/leeco-le-pro3.html) or with images [here](https://www.hardreset.info/devices/leeco/leeco-le-pro3/recovery-mode/).
- Check if the device is visible when running `sudo fastboot devices`
- Now check if the device is rooted: `sudo fastboot oem device-info`

After you installed android tools, you will have `adb` and `fastboot` commands available.

The command from above will generate something like this:

```bash
(bootloader)   Device tampered: false
(bootloader)    Device unlocked: true
(bootloader)    Device critical unlocked: true
(bootloader)    Charger screen enabled: false
(bootloader)    Serial console enabled: false
(bootloader)    Serial hw output enabled: false
(bootloader)    Display panel:
```

If the line `Device unlocked` is set to true, Ex: `Device unlocked: true` it means that the device is already rooted. But if you have this line: `Device unlocked: false`it means that the device is not rooted.

### **Root the phone**

The next steps should be made only if you had this line ``Device unlocked: false``.

`Start quote` source: [here](https://forum.xda-developers.com/le-pro3/leeco-le-pro-3-elite-model-x722-guides-development--and-mods/guide-unlock-bootloader-twrp-root-t3728582) . The next few paragraphs were copy-pasted, because I haven\'t tested them, my phone was already rooted so I didn\'t had to do these things, but I pasted them here just in case someone needs them. You can also check the original thread.

**Downgrade to 01D Rom**
 In some EUI roms you don\'t have to downgrade to 01D rom, but if you don\'t know if your rom can be unlocked or if you wanna feel more confortable and to avoid problems, is better to downgrade
 -firmware WIXCNFN5802005041S (see Settings > About Phone to confirm whether you\'re on this firmware or a different one... if you\'re on a different firmware on 20s, you don\'t need to downgrade)
 -stock 26s (if your x722 came with 26s, you need to downgrade to 01D)

 you can find the file here: [Download](https://www.androidfilehost.com/?fid=746010030569951596)
 Basically:
**Attetion!!! This update is to be used in the official recovery, TWRP will hard brick, don\'t flash it with TWRP**

- Put update.zip file inside your sdcard
- Reboot to recovery(steps above)
- Click on install update, and 01D rom will be installed
- Now you can reboot to fastboot mode and unlock your bootloader
- In fastboot mode type `fastboot oem unlock-go`
- Check if the phone is rooted with the steps from above.

Now, under Stock 20S unlocked folder you\'ll have a stock rom, cleaned, unlocked and friendly to root, you can intall in fastboot by opening flash.bat

`End quote`

### **Flash TWRP**

- Download TWRP from [here](https://sourceforge.net/projects/lineageos-on-leeco/files/twrp/zl0/)
- Boot your phone to Recovery Mode(`adb reboot bootloader` or Buttons `Volume Up` + `Power` pressed until something appears on the screen.)
- Flash the `twrp` image: `sudo fastboot flash recovery ./twrp-3.2.3-0-20181031-codeworkx-zl0.img`
- Boot again to Recovery Mode: `sudo fastboot boot ./twrp-3.2.3-0-20181031-codeworkx-zl0.img`(Or using the buttons like I said above). You should get the `twrp` menu.
- Voila
- 

### **Flash AICP and Gapps**

Here is the original thread: [https://forum.xda-developers.com/le-pro3/development/rom-aicp-13-0-pro3-aka-zl1-x727-x720-t3698058](https://forum.xda-developers.com/le-pro3/development/rom-aicp-13-0-pro3-aka-zl1-x727-x720-t3698058)

- Download the latest version of AICP
- Download Gapps from [here](https://opengapps.org/), choose mini or pico(the difference between them is the google apps, you can download the changelogs and compare them to see.)
- Deactivate USB debugging
- Copy archives on your phone via USB(you may need to connect the phone to a windows PC and install the Qualcomm USB drivers: [here](https://gsmusbdrivers.com/download/android-qualcomm-usb-driver/) or [here](https://androidurdu.net/download-qualcomm-usb-drivers-for-pc-windows/))
- Activate USB debugging
- Reboot into Recovery Mode(buttons or `adb reboot bootloader` and `sudo fastboot boot ./twrp-3.2.3-0-20181031-codeworkx-zl0.img`)
- Wipe the System, Cache, Data and ART/Dalvik cache.
- Go to install and search for your AICP archive and swipe to install it.
- Go back to install and search for `Gapps` archive and swipe to install it.
- You can flash the `Magisk` app here, I covered them below. It\'s better if you do them here. Short: Copy archive and install it.
- Reboot the system(Optional: Do not install the TWRP app when asked).

### **Flash and install Magisk**

- Check the main thread from [XDA formus](https://forum.xda-developers.com/apps/magisk/official-magisk-v7-universal-systemless-t3473445) and download the latest version, the zip archive.
- Deactivate USB debugging
- Copy it on your phone via USB(you may need to connect the phone to a windows PC and install the Qualcomm USB drivers: [here](https://gsmusbdrivers.com/download/android-qualcomm-usb-driver/) or [here](https://androidurdu.net/download-qualcomm-usb-drivers-for-pc-windows/))
- Activate USB debugging
- Reboot into Recovery Mode(buttons or `adb reboot bootloader` and `sudo fastboot boot ./twrp-3.2.3-0-20181031-codeworkx-zl0.img`)
- Go to install and search for your archive, swipe to install it and reboot the system.
- After reboot, open the `Magisk` app, it will ask you to install the full version, do it.
- After everything is done, you will notice that it installed `Magisk Manager` app, and it removed the `Magisk` app.
- Open `Magisk Manager` and click on install `Magisk`, choose the zip version, it will download it.
- Reboot into recovery mode and install again the `Magisk` zip archive.
- Reboot the system.
- Voila.