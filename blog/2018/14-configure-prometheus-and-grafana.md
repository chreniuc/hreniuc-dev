---
slug: configure-prometheus-and-grafana
title: Configure prometheus and grafana
authors: [cristi]
tags: [grafana, prometheus, step by step]
date: 2018-12-17T21:14
---

# Configure prometheus and grafana

## Prometheus

To run a container with prometheus, we need two things:

- `prometheus.yml` file
- Docker volume

The `prometheus.yml` file should contain something like this:

```bash
# my global config
global:
  scrape_interval:     5s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 5s # Evaluate rules every 15 seconds. The default is every 1 minute.

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: 'prometheus'
    static_configs:
    - targets: ['localhost:9090']

  - job_name: 'docker'
    static_configs:
    - targets: ['localhost:9999']

  - job_name: 'other_collector'
    scrape_interval: 1s
    static_configs:
      - targets: ['192.168.1.28:8000']

```

To create a docker volume, run:

```bash
docker create volume prometheus_data
```

This way, your collected data won\'t be lost on restarts.

```bash
docker run -d --restart=always --name=prometheus --network=host -v /path/to/prometheus.yml:/etc/prometheus/prometheus.yml -v prometheus_data:/prometheus prom/prometheus
```

You should open the browser and check if everything works, by default it is `9090` port.

## Grafana

For grafana we will also use a docker volume:

```bash
docker create volume grafana_data
```

This way we will save all data from grafana(panels, users, etc...).

To run the grafana container, simply run:

```bash
docker run -d --name=grafana --restart=always --network=host -e GF_SERVER_ROOT_URL=http://grafana.server.name -e GF_SECURITY_ADMIN_PASSWORD=secret -v grafana_data:/var/lib/grafana grafana/grafana
```

You can check it on port `3000`.

Now you should add the Prometheus server as a data source. You can do this by going to `Configuration` -> `Datasources` -> Select `Prometheus`