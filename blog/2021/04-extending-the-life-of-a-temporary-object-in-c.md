---
slug: extending-the-life-of-a-temporary-object-in-c
title: Extending the life of a temporary object in C++
authors: [cristi]
tags: [c++, extending life of a temporary, const, refference]
date: 2021-09-10T19:04
---

# Extending the life of a temporary object in C++

If you return a value from a method and you assing it to the a const reference, that value will be kept alive as long as the const reference is alive:

```c++
string get() { return "string"s; }

int main()
{
   const string& ref_to_temp = get();
   cout << ref_to_temp << endl;
}

```

Better explaination: https://herbsutter.com/2008/01/01/gotw-88-a-candidate-for-the-most-important-const/ or stackoverflow: https://stackoverflow.com/questions/2784262/does-a-const-reference-class-member-prolong-the-life-of-a-temporary

Or standard explaination: `8.5.3/5`, [here](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2005/n1905.pdf)