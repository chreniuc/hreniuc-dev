---
slug: archlinux-tips
title: ArchLinux tips
authors: [cristi]
tags: [archlinux, linux, pacman, tips]
date: 2019-05-23T19:13
---

# ArchLinux tips

Remove unused pacakges:

```bash
pacman -Rsn $(pacman -Qdtq)
```

List all unused packages:

```bash
pacman -Qdtq
```