---
slug: bruteforce-zip-password
title: Bruteforce zip password
authors: [cristi]
tags: [linux]
date: 2017-12-12T13:05
---

# Bruteforce zip password

I used [fcrackzip](http://manpages.ubuntu.com/manpages/xenial/man1/fcrackzip.1.html).

```bash
$ fcrackzip -D -p pass.txt -u file.zip
```

**-D** - dictionary mode, In this mode, **fcrackzip** will read passwords from a file, which must contain one password per line and should be alphabetically sorted (e.g. using sort(1)).

**-p** - Set initial (starting) password for brute-force searching to string, or use the file with the name string to supply passwords for dictionary searching.

**pass.txt** - file containing all passwords

**-u** - Try to decompress the first file by calling unzip with the guessed password. This weeds out false positives when not enough files have been given.

**file.zip** - the zip file on which you want to crack the password
