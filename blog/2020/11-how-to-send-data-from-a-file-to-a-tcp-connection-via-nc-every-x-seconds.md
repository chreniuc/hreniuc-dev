---
slug: how-to-send-data-from-a-file-to-a-tcp-connection-via-nc-every-x-seconds
title: How to send data from a file to a tcp connection via nc every x seconds
authors: [cristi]
tags: [bash, data, linux, mkfifo, nc, socket, tcp]
date: 2020-06-23T19:11
---

# How to send data from a file to a tcp connection via nc every x seconds

Source: https://stackoverflow.com/a/19271368

You can achieve it in two steps:

1) You need to start nc with a named pipe (fifo) as its input:

```bash
mkfifo /tmp/fifoIn; cat /tmp/fifoIn | nc localhost 2222 &
```

2) Send your data from file input.txt, line by line with 2 sec delay:

```bash
cat input.txt | while read line; do echo $line; sleep 2; done > /tmp/fifoIn
```

I've tested it with this "server" (I'm using openbsd netcat syntax):

```bash
nc -l localhost 2222
```

If you don't want the new-line char after every line use `echo -n` instead of `echo`.