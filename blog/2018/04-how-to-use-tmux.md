---
slug: how-to-use-tmux
title: How to use tmux
authors: [cristi]
tags: [how to, tmux, tmux keybindings,linux]
date: 2018-03-24T21:03
---

# How to use tmux

Resources: - [Getting started with tmux](https://linuxize.com/post/getting-started-with-tmux/#basic-tmux-usage) - [Youtube turorial 1](https://www.youtube.com/watch?v=Yl7NFenTgIo) - [Youtube turorial 2](https://www.youtube.com/watch?v=BHhA_ZKjyxo) Install **tmux** on Debian

```bash
sudo apt-get install tmux
```

You can also install [tmuxinator](https://github.com/tmuxinator/tmuxinator), which is easier to use, you can create session easily. **Tmux** has a lot of keybindings, those keybindings are in this format **CTRL + B [command]**. The **CTRL + B** is called the prefix, it like letting **tmux** know that the next key pressed is a command for it. You press **CTRL + B** release and then press a letter(**command**). Each tmux session has a number of **windows**, and each window has a number of **panes**. Create a new **tmux** session:

```bash
tmux new -s name
```

**Commands for windows: CTRL + B [command]**- **c** - Creates a new window.
- **,**(comma) - Gives you the possibility to rename the window.
- **p** - Go to the previous window.
- **n** - Go to the next window.
- **w** - List all windows(scrollable list of windows).
- **&** - Kill current window.
- **f** - Search for a window by name.

**Commands for panes: CTRL + B [command]**- **%** - Split vertically the current pane in two panes.
- **** - Split horizontally the current pane in two panes.
- **o** - Go to the next pane.
- **{** - Move pane to left with one position.
- **}**  - Move pane to right with one position.
- **q** - Show pane numbers + if you press a number, the pane with that number will become active(will have the cursor)
- **x** - Kill current pane.
- **z** - Zoom in the current pane.(It will enlarge it to the size of the whole terminal, to go back do the combination of keys again)

Move between **panes** : **CTRL + B** **left-arrow/right-arrow**/**up-arrow**/**down-arrow**Resize current pane: **CTRL + B** **left-arrow/right-arrow**/**up-arrow**/**down-arrow**(but do not release **CTRL**) Set a predefined layout: **CTRL + B** **Spacebar****Sessions:**- **CTRL + B** **D** - Detach from the session.

List all sessions:

```bash
tmux list-sessions
# or
tmux ls
```

Attach to a session:

```bash
tmux attach -t name_of_session
```

Running commands that do not have **keybindings**: **CTRL + B** :(two dots) **[text-command]:**- **split-window** - Splits the window horizontally
- **setw synchronize-panes on/off** - Write the same thing on all panes.
- **lock-client** - **follow the leader** All clients connected to tmux session will be on the same page, when one goes to another page, all go there.
- **lock-client off** - Disable **follow the leader**

To scroll through output history: > **Ctrl-b** then **[** then you can use your normal navigation keys to scroll around (eg. **Up Arrow** or **PgDn**). Press **q** to quit scroll mode. Alternatively you can press **Ctrl-b PgUp** to go directly into copy mode and scroll one page up (which is what it sounds like you will want most of the time)

More **keybindings** can be found here: [http://tmuxcheatsheet.com/](http://tmuxcheatsheet.com/)**Tips:**- To navigate through command history, use the mouse scroll

**Change the config of tmux:**- Create a config file in the home directory:

```bash
touch ~/.tmux.conf
```

- Add a keybinding to reload configuration while running tmux:

```bash
# reload config file (change file location to your the tmux.conf file)                                                         
bind-key r source-file ~/.tmux.conf\; display-message Config reloaded.
```

- Add everything you want there, layout design, keybindings, etc.

**Content of .tmux.conf:**

```bash
# Create tmux work-session or attach to it if it already exist

######################### Script tmux_start.sh begin ###########################
#!/bin/bash
# new-window command or use alias neww

# send-keys 'cd ~/data/project' C-m \; \
# Sends the command from '' and C-m will execute it.

docker_container_running=$(docker ps | grep container_name_2 | wc -l)
SESSIONNAME="work_session"
tmux has-session -t $SESSIONNAME &> /dev/null
if [ $? -ne 0 ]
then
  tmux new-session -s $SESSIONNAME\; \
  send-keys 'cd ~/data/project' C-m \; \
  rename-window "project" \; \
  new-window -c ~/data/project1 -n 'project1' \; \
  new-window -c ~/data/project2 -n "project2" \; \
  new-window -c ~/data/project3 -n 'project3' \; \
  new-window -c ~ -n 'home' \; \
  new-window -n 'qserver_qtcreator' 'xhost +; if [ $(docker ps | grep container_name | wc -l) -eq 0 ]; then docker start -i container_name; else docker exec -it container_name /bin/bash; fi' \; \
  split-window -h -c ~/data/project/source_1 \; \
  split-window -v -c ~/data/project/source_1 'xhost +; if [ $(docker ps | grep container_name | wc -l) -eq 0 ]; then docker start container_name; fi; docker exec -it -e QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb container_name qtcreator' \; \
  select-window -t "project_qtcreator"
else
  tmux a -t $SESSIONNAME
fi
################################### End Script #################################

# Add alias command work-session in ~/.bashrc
alias work-session='bash ~/data/tmux_start.sh'

# Reload configuration
source ~/.bashrc
```