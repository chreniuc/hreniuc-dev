---
slug: vscodium-change-default-config-folder
title: VSCodium change default config folder
authors: [cristi]
tags: [config folder, linux, vscodium]
date: 2019-06-21T19:20
---

# VSCodium change default config folder

[VSCodium](https://github.com/VSCodium/vscodium)


Modify the default config location:

```bash
ln -s  /home/chreniuc/data/configs/programs_config/VSCodium $HOME/.config/VSCodium
```