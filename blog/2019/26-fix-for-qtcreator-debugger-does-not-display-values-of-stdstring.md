---
slug: fix-for-qtcreator-debugger-does-not-display-values-of-stdstring
title: Fix for QtCreator debugger does not display values of std::string
authors: [cristi]
tags: [gcc, c++, gcc 9.1.0, not accessible, Qtcreator, std::String]
date: 2019-08-08T19:26
---

# Fix for QtCreator debugger does not display values of std::string

We encountered a problem when using the debugger from qtcreater with the gcc 9.1.0. The values of the `std::string` were not printed, they were marked as : `<not accessible>`. We found the fix on [stackoverflow](https://stackoverflow.com/a/57187409).

The fix is simple, you need to change the file: `/usr/share/qtcreator/debugger/stdtypes.py` as shown in the diff:

```diff
640c640
< (size, alloc, refcount) = d.split("ppp", value.address() + d.ptrSize())
---
> (size, alloc, refcount) = d.split("ppp", data - 3 * d.ptrSize())
643,644c643
< if size > 1000:
< size = 1000
---
> d.check(0 <= size and size <= alloc and alloc <= 100*1000*1000)
```