---
slug: useful-things-for-editing-pdfs-from-terminal
title: Useful things for editing pdfs from terminal
authors: [cristi]
tags: [180, linux, multiple pdf, pdftk, pdfunite.pdf merge, rotate pdf]
date: 2019-07-22T19:23
---

# Useful things for editing pdfs from terminal

Merge multiple pdfs into one on linux:

[Source](https://unixblogger.com/how-to-easily-merge-pdf-documents-under-linux/)

Use `poppler-utils` or `poppler` package, which comes with multiple tools. The one we are going to use is `pdfunite`.

Example:

```bash
pdfunite 1.pdf 2.pdf 3.pdf 4.pdf 5.pdf 6.pdf out.pdf
```

*Note: Do not forget to add the last parameter which is the output file. If you don't give it, it will override the last input file that you give.*

___

Using `pdftk`:

```bash
pdftk.pdf 2.pdf 3.pdf 4.pdf 5.pdf 6.pdf  cat output out.pdf
```

**Rotate 180:**

```bash
pdftk pdf1.pdf cat 1-endsouth output rotated_pdf1.pdf
```

Source: [here](https://www.pdflabs.com/docs/pdftk-cli-examples/)