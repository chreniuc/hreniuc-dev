---
slug: create-new-php-file-from-wordpress-editor
title: Create new php file from wordpress editor
authors: [cristi]
tags: [wordpress]
date: 2017-08-15T13:00
---

# Create new php file from wordpress editor

Useful when you do not have access to the hosting account:

```php
<?php touch('wp-content/themes/YOUR_THEME_DIR/FILE_NAME.php');?>
```
