---
slug: linux-bash-tips-and-tricks
title: Linux bash tips and tricks
authors: [cristi]
tags: [bash, linux, tips, tricks]
date: 2020-05-22T19:06
---

# Linux bash tips and tricks

**1. Get pid of specific executable:**

```bash
pgrep exe_name
```