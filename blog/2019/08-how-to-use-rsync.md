---
slug: how-to-use-rsync
title: How to use rsync
authors: [cristi]
tags: [linux, rsync]
date: 2019-03-26T19:08
---

# How to use rsync


Copy files from a different server and preserv file owners, permissions.

```bash
rsync -avprog user@dns:/path/scripts destination
```

 - `a` - Archive, don't know what it does
 - `v` - Verbose
 - `p` - Preserve file permissions
 - `r` - Reccursive
 - `o` - Preserve file owners *Needs root*
 - `g` - Preserve file group *Needs root*