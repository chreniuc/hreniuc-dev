---
slug: thread-sanitizer-tips-n-tricks
title: Thread sanitizer tips n tricks
authors: [cristi]
tags: [c++, thread, sanitizer]
date: 2020-06-22T19:10
---

# Thread sanitizer tips n tricks

Common flags for sanitizers, [here]( https://github.com/google/sanitizers/wiki/SanitizerCommonFlags)

1. Redirect report to specific file:

```bash
export TSAN_OPTIONS="log_path=tsan_output.log"
./executable
```

All options that can be set for thread sanitizer can be found [here]( https://github.com/google/sanitizers/wiki/ThreadSanitizerFlags)