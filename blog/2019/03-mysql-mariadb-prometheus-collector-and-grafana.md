---
slug: mysql-mariadb-prometheus-collector-and-grafana
title: Mysql/MariaDB Prometheus collector and Grafana
authors: [cristi]
tags: [grafana, mariadb collector, prometheus]
date: 2019-02-05T19:03
---

# Mysql/MariaDB Prometheus collector and Grafana

First, download the prometheus collector from their webpage: https://github.com/prometheus/mysqld_exporter or use docker.

Next you need to add an new user in the database:

```sql
CREATE USER 'mysqld_exporter'@'%' IDENTIFIED BY 'password' WITH MAX_USER_CONNECTIONS 3;

GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'mysqld_exporter'@'%';
```

I used `%` for host of the user, to allow logging in from everywhere. I did this because I encountered some errors when I used `localhost`: `ERRO[0026] Error pinging mysqld: dial tcp :0: connect: connection refused  source="exporter.go:119" `

```bash
export DATA_SOURCE_NAME='mysqld_exporter:password@(127.0.0.1:3306)/' && \
  nohup ./mysqld_exporter --no-collect.info_schema.tables --no-collect.slave_status > nohup.out 2>&1 &
```

Use this dashboard from grafana: https://grafana.com/dashboards/7362