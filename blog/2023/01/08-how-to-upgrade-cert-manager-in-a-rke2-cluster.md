---
slug: how-to-upgrade-cert-manager-in-a-rke2-cluster
title: How to upgrade cert-manager in a rke2 cluster
authors: [cristi]
tags: [kubernetes, rke, rke2, update, cert-manager,upgrade ]
date: 2023-01-06T01:01
---

# How to upgrade cert-manager in a rke2 cluster

As stated [here](https://cert-manager.io/docs/installation/upgrading/#upgrading-with-helm), but make sure there are no other specific things for upgrading from a version to another.

**Upgrade CRDs**

```bash
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.8.2/cert-manager.crds.yaml
```

**Upgrade cert-manager**

```bash
helm upgrade --version 1.8.2 cert-manager jetstack/cert-manager --namespace cert-manager
```

**Check the pods**

```bash
kubectl get pods --namespace cert-manager
```