---
slug: preparing-c-dev-infrastructure
title: Preparing C++ Dev infrastructure
authors: [cristi]
tags: [c++, dev, enviroment, cmake, conan, gcc, clang, qtcreator]
date: 2022-03-30T19:02
---

# Preparing C++ Dev infrastructure

## Tools

 - [Conan Package Manager](https://conan.io/)
 - CMake
 - gcc + make
 - clang (for autoformat)
 - [QtCreator](https://www.qt.io/product/development-tools) as IDE

```bash
# Make sure pip is using python3
pip install conan

# Cmake
sudo apt-get install cmake

# gcc + make
sudo apt-get install build-essential
```

Install clang for automatic formatting:

```bash
#!/bin/bash

# Use this script to install the clang formatter and all its utilities.
# Install Clang(Used for clang-format)
# ============================================================================ #
mkdir -p ~/programs/clang && cd ~/programs/clang &&
# http://releases.llvm.org/download.html
clang_version="9.0.0" &&
archive_name="clang+llvm-${clang_version}-x86_64-linux-gnu-ubuntu-18.04" &&
wget http://releases.llvm.org/${clang_version}/${archive_name}.tar.xz &&
tar xf ${archive_name}.tar.xz &&
rm -Rf ${archive_name}.tar.xz &&
unlink clang_latest # Just in case there was another sym link
ln -sf ${archive_name} clang_latest &&
(
  echo "CLANG_INSTALL_PATH=\"/mnt/programs/clang/clang_latest\"" &&
  echo "export PATH=\"\${CLANG_INSTALL_PATH}/bin:\$PATH\"" &&
  echo -n "export LD_LIBRARY_PATH=\"\${CLANG_INSTALL_PATH}/lib" &&
  echo ":\$LD_LIBRARY_PATH\""
) >> ~/.bashrc
# ============================================================================ #
```

___

### IDE

Use [QtCreator](https://www.qt.io/product/development-tools).

Activate these plugins:

```bash
Go to Help -> About plugins:
  - C++
    - Check ClangCodeModel
    - Check ClangFormat
    - Beautifier
  - Code Analyzer
    - Check ClangTools
  - Utilities
    - Check Todo
```

Set these settings:

```bash
Go to Tools -> Options:
  - Text Editor
    - Behavior:
      - Tab policy: Spaces Only
      - Tab size: 2
      - Indend size: 2
      - Align continuation lines: With Spaces
      - Clean whitespace (upon saving)
    - Display:
      - Display right margin at column: 80
      - Display line numbers
      - Display folding markers
      - Visualize whitespace
      - Highlight search results on the scrollbar
      - Highlight current line
      - Highlight blocks
      - Animate matching parentheses
      - Highlight matching parentheses
    - Completion:
      - Enable Doxygen blocks
      - Generate brief description
      - Add leading asterisks
  - Beautifier
    - Enable auto format on file save
	- Clang Format
	  - Use predefined style: `File`
 - Debugger
   - General:
     - Set breakpoints using full absolute path
```


