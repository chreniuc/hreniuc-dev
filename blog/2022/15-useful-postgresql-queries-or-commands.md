---
slug: useful-postgresql-queries-or-commands
title: Useful PostgreSQL queries or commands
authors: [cristi]
tags: [postgresql, sql, queries, psql]
date: 2022-09-23T18:56
---

# Useful PostgreSQL queries or commands

Useful documentation with examples:

- [Administration](https://www.postgresqltutorial.com/postgresql-administration/)
- [Queries](https://www.postgresqltutorial.com/)
- [Official Docs](https://www.postgresql.org/docs/current)

**Connect to the postgresql**

```bash
psql -p 5432 -h localhost -U postgres -W
```

## Database and Schema

**Show all databases**

```sql
\l
# Or
SELECT datname FROM pg_database;
```

**Switch to another database**

Similar to `USE` from mysql.

```sql
\c databasename
```

**Create database**

[Documentation](https://www.postgresql.org/docs/current/sql-createdatabase.html)

```sql
CREATE DATABASE test_db;
```

**Drop/Delete database**

[Documentation](https://www.postgresql.org/docs/current/sql-dropdatabase.html)

```sql
DROP DATABASE IF EXISTS test_db;
```

A database may contain multiple schemas, which groups tables from a database. By default `public` schema is created for every database. And also you don't have to specify it in your queries, eg: `public.staff` is the same as `staff`. More examples and documentation can be checked [here](https://www.postgresqltutorial.com/postgresql-administration/postgresql-schema/)

## User/Role

**Show all users**

```sql
\du
# With description
\du+
# Or sql
SELECT rolname FROM pg_roles;
```

**Create a user**

[Documentation](https://www.postgresql.org/docs/current/sql-createrole.html)

```sql
CREATE USER test_user WITH PASSWORD 'pass';
```

**Grant user rights to connect to db**

```sql
GRANT CONNECT ON DATABASE test_db TO test_user;
```

Try to connect afterwards:

```bash
psql -p 5432 -h localhost -U test_user -W -d test_db
```

**Create superuser**

[Documentation](https://www.postgresql.org/docs/current/sql-createrole.html)

```sql
CREATE ROLE test_user2 WITH LOGIN SUPERUSER CREATEDB PASSWORD 'pass';
# Connect afterwards
psql -p 5432 -h localhost -U test_user2 -W -d postgres
```

**Alter user, add createdb rights**

```sql
ALTER ROLE test_user2 WITH CREATEDB;
```

**Grant all privileges on the database**

[Documentation](https://tableplus.com/blog/2018/04/postgresql-how-to-grant-access-to-users.html)

```sql
GRANT ALL PRIVILEGES ON DATABASE test_db TO test_user2;
```

**Common users**

You ussually need the following users:

**Reader:**

```sql
CREATE USER reader_user WITH PASSWORD 'reader_user';
GRANT CONNECT ON DATABASE test_db TO reader_user;

\c databsename
ALTER DEFAULT PRIVILEGES
FOR USER reader_user
IN SCHEMA public
GRANT SELECT ON TABLES TO reader_user;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT USAGE, SELECT ON SEQUENCES TO reader_user;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO reader_user;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO reader_user;
```

**Writer:**

```sql
CREATE USER reader_writer WITH PASSWORD 'reader_writer';
GRANT CONNECT ON DATABASE test_db TO reader_writer;

\c databsename
ALTER DEFAULT PRIVILEGES
FOR USER reader_writer
IN SCHEMA public
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO reader_writer;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT USAGE, SELECT ON SEQUENCES TO reader_writer;

GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO reader_writer;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO reader_writer;
```

**Admin**

```sql
CREATE USER admin WITH PASSWORD 'admin';
GRANT CONNECT ON DATABASE test_db TO admin;

\c test_db
ALTER DEFAULT PRIVILEGES
FOR USER admin
IN SCHEMA public
GRANT ALL ON TABLES TO admin;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT ALL ON SEQUENCES TO admin;

GRANT ALL ON ALL TABLES IN SCHEMA public TO admin;
GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO admin;
```

## Tables

**Create table**

[Documentation](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-create-table/), [official documentation](https://www.postgresql.org/docs/current/sql-createtable.html)

```sql
CREATE TABLE accounts (
	user_id serial PRIMARY KEY,
	username VARCHAR ( 50 ) UNIQUE NOT NULL,
	password VARCHAR ( 50 ) NOT NULL,
	email VARCHAR ( 255 ) UNIQUE NOT NULL,
	created_on TIMESTAMP NOT NULL,
  last_login TIMESTAMP
);

CREATE TABLE address (
	id serial PRIMARY KEY,
  name VARCHAR ( 50 ) NOT NULL
);

CREATE TABLE people (
	id serial PRIMARY KEY,
  name VARCHAR ( 50 ) NOT NULL
);
```

**Describe table**

[Documentation](https://www.postgresqltutorial.com/postgresql-administration/postgresql-describe-table/)

```sql
\d accounts
```

**Show tables**

[Documentation](https://www.postgresqltutorial.com/postgresql-administration/postgresql-show-tables/)

```sql
\dt+
```

**Grant SELECT on table**

[Documentation](https://www.postgresqltutorial.com/postgresql-administration/postgresql-grant/), [Official docs](https://www.postgresql.org/docs/current/sql-grant.html)

First you need to connect with an user that has grant privileges and then switch to the database that contains that table:

```sql
\c test_db
GRANT SELECT ON accounts TO test_user;
```

**Grant select on all tables even when new tables are added\***

[Documentation](https://tableplus.com/blog/2018/04/postgresql-how-to-create-read-only-user.html), [official documentation](https://www.postgresql.org/docs/current/sql-alterdefaultprivileges.html)

```sql
ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT SELECT ON TABLES TO test_user;
```

**See rights for specific table**

[Docs](https://www.postgresql.org/docs/current/ddl-priv.html)

```sql
\dp table
# Default priv
\ddp table
```
