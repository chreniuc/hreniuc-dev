---
slug: extract-deb-files
title: Extract deb files
authors: [cristi]
tags: [debian, extract, linux]
date: 2019-06-21T19:18
---

# Extract deb files

Here's the command:

```bash
dpkg -x package.deb
```