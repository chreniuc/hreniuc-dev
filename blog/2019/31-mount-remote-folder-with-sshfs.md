---
slug: mount-remote-folder-with-sshfs
title: Mount remote folder with sshfs
authors: [cristi]
tags: [linux, mount remote folder, sshfs]
date: 2019-09-28T19:31
---

# Mount remote folder with sshfs

As root user:

```bash
cd /mnt
mkdir mount_folder

sshfs -o allow_other  user@ip:/remote_folder /mnt/mount_folder/

# -o allow_other -> Needed so every user can see the folder.
```