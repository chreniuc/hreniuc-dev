---
slug: automate-ssl-certificate-creation-or-renewal-with-nginx-and-certbot-using-docker-compose
title: "Automate ssl certificate creation or renewal with nginx and certbot using docker compose"
authors: [cristi]
tags: [docker, docker-compose, nginx, certbot, nginx-certbot, ssl]
date: 2023-01-21T20:01
---

# Automate ssl certificate creation or renewal with nginx and certbot using docker compose

To do so, I used [jonasal/nginx-certbot](https://github.com/JonasAlfredsson/docker-nginx-certbot/blob/master/docs/good_to_know.md) docker image.

My docker compose file looked like this:

```yml
version: "3"
name: "nginx"
services:
  nginx:
    image: jonasal/nginx-certbot:3.3.1
    container_name: nginx_prod_1
    ports:
      - 80:80
      - 443:443
    volumes:
      - ./fs/nginx/secrets:/etc/letsencrypt:rw
      - ./fs/nginx/log:/var/log/nginx:rw
      - ./fs/nginx/user_conf.d:/etc/nginx/user_conf.d:ro
    environment:
      DEBUG: 1
      CERTBOT_EMAIL: email
      #STAGING: 1
    restart: always
```

<!-- truncate -->

As you can see, I had the following folder structure:

```bash
.
├── docker-compose.yml
└── fs
    └── nginx
        ├── log
        ├── secrets
        └── user_conf.d
            ├── domain.tld.conf
```

For the container to create and renew a certificate, you need to create a conf file in user_conf.d, something like this:

```bash
upstream my-proxy {
  server localhost:6070;
}

server {
    # Listen to port 443 on both IPv4 and IPv6.
    listen 443 ssl;
    listen [::]:443 ssl;

    # Domain names this server should respond to.
    server_name domain.tld www.domain.tld;

    # Load the certificate files.
    ssl_certificate         /etc/letsencrypt/live/domain.tld/fullchain.pem;
    ssl_certificate_key     /etc/letsencrypt/live/domain.tld/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/domain.tld/chain.pem;

    # Load the Diffie-Hellman parameter.
    ssl_dhparam /etc/letsencrypt/dhparams/dhparam.pem;

    location / {
      # Optional
      proxy_set_header   Host             $host;
      proxy_set_header   X-Real-IP        $remote_addr;
      proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
      proxy_pass http://my-proxy;
    }
}
```