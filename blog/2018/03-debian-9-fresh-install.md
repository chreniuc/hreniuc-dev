---
slug: debian-9-fresh-install
title: Debian 9 fresh install
authors: [cristi]
tags: [linux,debian]
date: 2018-03-16T21:02
---

# Debian 9 fresh install

### Initial packages

```bash
apt-get install gcc g++ make cmake gedit network-manager-gnome tmux gedit-plugins
```

Remove **wicd** package, because it doesn\'t detect the wi-fi networks automatically. That\'s why i installed **network-manager-gnome** package.

```bash
apt-get remove wicd
apt-get autoremove
```

### Fix wifi driver issue on Debian 9

The error i was getting was: *firmware: failed to load iwlwifi-8000C-23.ucode (-2)*I was able to fix it using an answer on [stackoverflow](https://stackoverflow.com/a/47488591)Those were the steps i had to follow: 1. Add **non-free** and **contrib** components to **/etc/apt/sources.list**

```bash
deb http://ftp.ro.debian.org/debian/ stretch main contrib non-free
deb-src http://ftp.ro.debian.org/debian/ stretch main contrib non-free
```

**Run**

```bash
sudo apt-get update
apt-get install firmware-iwlwifi
```

**Reboot**

### Fix TPM error

Error con*tent : tpm tpm0: A TMP error occurred attempting to read a pcr value***Fix**: Disable **TPM** from **BIOS**. > Trusted Platform Module (TPM) is an international standard for a secure cryptoprocessor, which is a dedicated microprocessor designed to secure hardware by integrating cryptographic keys into devices. In practice a TPM can be used for various different security applications such as secure boot and key storage. TPM is naturally supported only on devices that have TPM hardware support. If your hardware has TPM support but it is not showing up, it might need to be enabled in the BIOS settings.