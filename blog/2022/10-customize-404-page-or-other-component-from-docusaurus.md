---
slug: customize-404-page-or-other-component-from-docusaurus
title: Customize 404 page or other component from Docusaurus
authors: [cristi]
tags: [docusaurus, personal-website, 404, component, swizzle]
date: 2022-09-19T20:20
---

# Customize 404 page or other component from Docusaurus

You need to `swizzle` the component and customize it as stated [here](https://docusaurus.io/docs/swizzling#swizzling-theme-components).

Run:

```bash
yarn run swizzle @docusaurus/theme-classic NotFound
```

I've used `eject` mode, which creates a copy.

And then edit the `src/theme/NotFound.js` file.

To see which components can be modified run the following:

```bash
yarn run swizzle -- --list
```