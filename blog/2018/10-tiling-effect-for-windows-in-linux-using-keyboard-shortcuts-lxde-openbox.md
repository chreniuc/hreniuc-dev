---
slug: tiling-effect-for-windows-in-linux-using-keyboard-shortcuts-lxde-openbox
title: Tiling effect for windows in linux using keyboard shortcuts, lxde, openbox
authors: [cristi]
tags: [debian, lxde, maximize half bottom, maximize half left, maximize half right, maximize half upper, openbox, tilling]
date: 2018-07-17T21:10
---

# Tiling effect for windows in linux using keyboard shortcuts: lxde, openbox

**LE: 14th Sept 2022 - Ubuntu 20.04 supports most of these out of the box now, with other keybinings.**

Add these in :`~/.config/openbox/lxde-rc.xml` or `~/.config/openbox/lubuntu-rc.xml` :

```xml
<!-- Keybindings for window tiling -->

    <keybind key=C-W-Up>        # FullScreen
      <action name=Maximize/>
    </keybind>
    <keybind key=C-W-Down>        # MiddleScreen
      <action name=UnmaximizeFull/>
      <action name=MoveResizeTo><x>center</x><y>center</y><width>50%</width><height>50%</height></action>
    </keybind>

    <keybind key=C-W-Left>        # HalfLeftScreen
      <action name=UnmaximizeFull/>
      <action name=MoveResizeTo><x>0</x><y>0</y><height>100%</height><width>50%</width></action>
    </keybind>
    <keybind key=C-W-Right>        # HalfRightScreen
      <action name=UnmaximizeFull/>
      <action name=MoveResizeTo><x>-0</x><y>0</y><height>100%</height><width>50%</width></action>
    </keybind>

    <keybind key=C-W-1>        # UpperQuarterLeft
      <action name=UnmaximizeFull/>
      <action name=MoveResizeTo><x>0</x><y>0</y><height>50%</height><width>50%</width></action>
    </keybind>

    <keybind key=C-W-2>        # LowerQuarterLeft
      <action name=UnmaximizeFull/>
      <action name=MoveResizeTo><x>0</x><y>-0</y><height>50%</height><width>50%</width></action>
    </keybind>

    <keybind key=C-W-3>        # LowerQuarterRight
      <action name=UnmaximizeFull/>
      <action name=MoveResizeTo><x>-0</x><y>-0</y><height>50%</height><width>50%</width></action>
    </keybind>

    <keybind key=C-W-4>        # UpperQuarterRight
      <action name=UnmaximizeFull/>
      <action name=MoveResizeTo><x>-0</x><y>0</y><height>50%</height><width>50%</width></action>
    </keybind>
```

Add those inside `<keyboard></keyboard>`tags. Then run: `openbox --reconfigure`[Adding a shortcut in lxde](http://xahlee.info/linux/linux_lxde_add_key_shortcuts.html)