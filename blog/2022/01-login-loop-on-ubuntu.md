---
slug: login-loop-on-ubuntu
title: Login Loop on Ubuntu
authors: [cristi]
tags: [login loop, regolith, ubuntu, linux]
date: 2022-01-31T19:01
---

# Login Loop on Ubuntu

```bash
sudo apt update
sudo apt upgrade
sudo apt autoremove
sudo apt-get remove --purge nvidia*
sudo apt-get remove --purge "nvidia*"
sudo apt install nvidia-driver-470      (you may need other version)
sudo reboot
```
Source: https://askubuntu.com/a/1362971