---
slug: increase-swap-size-after-install-ubuntu-20
title: Increase swap size after install, Ubuntu 20
authors: [cristi]
tags: [increase, linux, swap, ubuntu]
date: 2022-06-27T19:07
---

# Increase swap size after install, Ubuntu 20

Source: https://askubuntu.com/a/1264577

In sleep mode, the content of ram is kept as it is, and the computer works on a very low power mode, so as to keep the ram content intact (as ram will lose the data if power supply is cut to it). But in hibernation, the ram content is stored in the swap space, so power can be completely cut off. Hence it is recommended to have swap size as large as the ram size.

<!--truncate-->

1. First disable the swap and delete it  

```bash
sudo swapoff /swapfile  
sudo rm  /swapfile
```

2. Create new swap space of size 16 GB (16 * 1024 = 16384). `bs` is the block size. Basically bs * count = bytes to be allocated (in this case 16 GB). Here bs = 1M (M stands for mega, so we are assigning 1MB block size) and we are allocating 16384 * 1MB (=16GB) to swap.

```bash
sudo dd if=/dev/zero of=/swapfile bs=1M count=16384
```

3. Give it the read/write permission for root

```bash
sudo chmod 600 /swapfile
```

4. Format it to swap

```bash
sudo mkswap /swapfile
```

5. Turn on swap again

```bash
sudo swapon /swapfile
```

6. Now reboot the PC for the above changes to take place.