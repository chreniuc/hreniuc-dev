---
slug: create-inner-join-in-typeorm-without-explicit-relations-in-enitity
title: "Create inner join in TypeORM without explicit relations in Enitity"
authors: [cristi]
tags: [typescript, nodejs, typeorm, sql]
date: 2024-03-01T18:01
---

# Create inner join in TypeORM without explicit relations in Enitity

When you have two tables and you want to create an inner join without having a relation in the entities, you can use the `innerJoin` method from the `createQueryBuilder` method like this:

```ts
.innerJoin(
        UserCompany,
        'user_company',
        'user_company.userId = notification_subscription.userId',
      )
```

This works even if you don't have a relation between the two entities.

```ts
const notifForCompanyManagers = await dbConnection.manager
      .getRepository(NotificationSubscription)
      .createQueryBuilder('notification_subscription')
      .select('notification_subscription.token')
      .addSelect('notification_subscription.userId')
      .innerJoin(
        UserCompany,
        'user_company',
        'user_company.userId = notification_subscription.userId',
      )
      .where(
        '(user_company.manager = true or user_company.owner = true) and  user_company.companyId = :companyId',
        { companyId: companyId },
      )
      .getMany();
```