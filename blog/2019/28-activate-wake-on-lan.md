---
slug: activate-wake-on-lan
title: Activate wake on LAN
authors: [cristi]
tags: [debian, internet, linux, wake on lan, wol]
date: 2019-09-03T19:28
---

# Activate wake on LAN

[Here](https://www.cyberciti.biz/tips/linux-send-wake-on-lan-wol-magic-packets.html) is the source.

Activate WOL in bios:

>First, reboot the remote server and go to BIOS > Power Management > “Wake On LAN”. Turn it on. Next, save and close the bios. 

Install `ethtool`:

```bash
apt-get install ethtool
```

Enable:

```bash
ethtool -s enp9s0 wol g
# ethtool -s <interface> wol g
```

Check if enabled:

```bash
ethtool enp9s0
# ethtool <interface>
# Output:

Supported ports: [ TP MII ]
Supported link modes:   10baseT/Half 10baseT/Full 
100baseT/Half 100baseT/Full 
1000baseT/Full 
Supported pause frame use: Symmetric Receive-only
Supports auto-negotiation: Yes
Supported FEC modes: Not reported
Advertised link modes:  10baseT/Half 10baseT/Full 
100baseT/Half 100baseT/Full 
1000baseT/Full 
Advertised pause frame use: Symmetric Receive-only
Advertised auto-negotiation: Yes
Advertised FEC modes: Not reported
Link partner advertised link modes:  10baseT/Half 10baseT/Full 
100baseT/Half 100baseT/Full 
1000baseT/Full 
Link partner advertised pause frame use: Symmetric
Link partner advertised auto-negotiation: Yes
Link partner advertised FEC modes: Not reported
Speed: 1000Mb/s
Duplex: Full
Port: MII
PHYAD: 0
Transceiver: internal
Auto-negotiation: on
Supports Wake-on: pumbg
Wake-on: g
Current message level: 0x00000033 (51)
drv probe ifdown ifup
Link detected: yes
```

The most important thins is to check these two lines(**g** should be present):

```bash
Supports Wake-on: pumbg
Wake-on: g
```

You should also make it run at boot-up. Add this is `/etc/rc.local`:

```bash
#!/bin/bash

ethtool -s enp9s0 wol g

exit 0
```

Then make that file executable:

```bash
chmod +x /etc/rc.local
```

Reboot and check if you get `Wake-on: g`. 

To access it from Internet, add a port forwarding in your router settings, to port 9 (UDP) on your PC.

Example if my PC has the ip `192.168.2.101` and the external port(from the router is `999`) it will look like this:

`public_ip:999 UDP` -> `192.168.2.101:9 UDP`.

___

Turn your pc from linux pc:

```bash
apt-get install wakeonlan
wakeonlan -i IP -p PORT MAC
```

From [android](https://play.google.com/store/apps/details?id=co.uk.mrwebb.wakeonlan).