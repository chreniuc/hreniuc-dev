---
slug: kubernetes-useful-commands
title: Kubernetes useful commands
authors: [cristi]
tags: [kubernetes, cluster, kubectl ]
date: 2023-01-06T06:01
---

# Kubernetes useful commands

*Make sure you have exported the KUBECONFIG env:* `export KUBECONFIG=/path/rke2_inventory/hetzner/credentials/rke2.yaml`

1. Get all pods and the nodes they are on

```bash
kubectl get pods -o wide
```

2. Get nodes and their labels

```bash
kubectl get nodes --show-labels
```

3. Deploy something to the cluster:

```bash
kubectl apply -f hetzner/domain.com-backend.yaml
```

4. Delete a deployment from the cluster:

```bash
kubectl delete -f https://k8s.io/examples/application/deployment.yaml
```

5. Get/watch logs from pod:

```bash
kubectl logs -f pod/vt-vttablet-decontabodusseldorf-2620423388-0c5af156
```

6. Delete pod:

```bash
kubectl delete pod/vt-vttablet-decontabodusseldorf-2620423388-0c5af156
```