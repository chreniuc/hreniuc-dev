---
slug: reload-audio-devices-on-ubuntu
title: Reload audio devices on Ubuntu
authors: [cristi]
tags: [pulseaudio, reload devices, ubuntu, linux]
date: 2021-06-18T19:03
---

# Reload audio devices on Ubuntu

Run this:

```bash
pacmd unload-module module-udev-detect && pacmd load-module module-udev-detec
```

[Source](https://superuser.com/a/869229)