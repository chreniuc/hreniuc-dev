---
slug: how-to-upgrade-rancher-in-a-rke2-cluster
title: How to upgrade rancher in a rke2 cluster
authors: [cristi]
tags: [kubernetes, rke, rke2, upgrade, rancher ]
date: 2023-01-06T02:01
---

# How to upgrade rancher in a rke2 cluster

As stated in these docs: https://rancher.com/docs/rancher/v2.5/en/installation/install-rancher-on-k8s/upgrades/#option-a-upgrading-rancher

```bash
sudo snap install helm --classic
```

Upgrade it:

```bash
# To have the same values:
helm get values rancher -n cattle-system -o yaml > rancher_values.yaml

# Add the repo
helm repo add rancher-stable https://releases.rancher.com/server-charts/stable

helm repo update

helm fetch rancher-stable/rancher


helm upgrade rancher rancher-stable/rancher \
  --namespace cattle-system \
  -f rancher_values.yaml \
  --version=2.6.5
```

If `helm upgrade` doesn't work we need to delete the rancher and reinstall it, following [these steps](https://rancher.com/docs/rancher/v2.5/en/installation/install-rancher-on-k8s/upgrades/#option-b-reinstalling-rancher-and-cert-manager) without the cert-manager:

```bash
helm delete rancher -n cattle-system

helm install rancher rancher-stable/rancher \
  --namespace cattle-system \
  -f rancher_values.yaml \
  --version=2.6.5
```

It should reuse the old configs and so.

Check the pods and logs:

```bash
kubectl get pods --namespace cattle-system

kubectl -n cattle-system rollout status deploy/rancher

kubectl logs -f rancher-84696c75d9-62mk4 --namespace cattle-system
# Should check the version
2022/07/01 08:49:27 [INFO] Rancher version v2.6.5 (c4d59fa88) is starting
```