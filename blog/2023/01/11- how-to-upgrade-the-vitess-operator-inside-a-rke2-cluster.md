---
slug: how-to-upgrade-the-vitess-operator-inside-a-rke2-cluster
title: How to upgrade the vitess operator inside a rke2 cluster
authors: [cristi]
tags: [kubernetes, rke, rke2, vitess, cluster, upgrade, vitess-operator ]
date: 2023-01-06T04:01
---

# How to upgrade the vitess operator inside a rke2 cluster

Check when a new version is released [here](https://github.com/planetscale/vitess-operator/releases) and check if it has been updated [here](https://github.com/vitessio/vitess/blob/v14.0.0/examples/operator/operator.yaml), usually they only change the version of the operator image in that file.


Usually we should keep them in sync, the vitess-operator image and the vitess image.

When upgrading make sure you specify here the latest version used, so it will be easier to see what version we are using:

```bash
kubectl apply -f https://raw.githubusercontent.com/vitessio/vitess/v14.0.0/examples/operator/operator.yaml

# Vitess operator 2.7.1

kubectl get pod
# Logs

kubectl logs -f vitess-operator-647f7cc94f-zgf9q
```

**Sometimes you might need to restart the backends, because this might trigger a restart for the vitess, and the backend sometimes doesn't refresh the connection ot the db. Check the logs of the backend.**
