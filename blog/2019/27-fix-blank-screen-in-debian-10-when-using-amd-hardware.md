---
slug: fix-blank-screen-in-debian-10-when-using-amd-hardware
title: Fix blank screen in Debian 10 when using amd hardware
authors: [cristi]
tags: [amdgpu, blank screen, debian 10, firmware-linux, linux]
date: 2019-08-18T19:27
---

# Fix blank screen in Debian 10 when using amd hardware

I recently installed Debian 10 and [liquorix](https://liquorix.net/)(latest kernel) when I booted a blank screen apeared.

Swithing back to the old kernel worked, but the resolution of my monitor was poor. I tried to see what is the issue and ran :

```bash
dmesg
```

In that output I noticed this line:

```bash
drm:amdgpu_pci_probe [amdgpu]] *ERROR* amdgpu requires firmware installed
```

I searched on google a fix and got to [this post.](https://linuxconfig.org/how-to-install-amdgpu-drivers-on-debian-9-stretch-linux)

The fix was simple, I added `contrib non-free` after `main` on every line from `/etc/apt/sources.list`. Then I ran:

```bash
apt-get update && apt-get upgrade
```

And then I installed `firmware-linux`:

```bash
apt-get install firmware-linux
```

And then I rebooted the PC.
