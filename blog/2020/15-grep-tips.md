---
slug: grep-tips
title: grep tips
authors: [cristi]
tags: [bash, grep, linux, tips]
date: 2020-07-14T19:15
---

# grep tips

Get lines that contain the same pattern twice or X times([source](https://unix.stackexchange.com/a/596143)):

```bash
grep -E "(string.*){2}" file.log
```