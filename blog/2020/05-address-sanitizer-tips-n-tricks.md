---
slug: address-sanitizer-tips-n-tricks
title: Address sanitizer tips and tricks
authors: [cristi]
tags: [c++, address sanitizer, sanitizer]
date: 2020-05-21T19:05
---

# Address sanitizer tips and tricks

**1. Redirect report to specific file:**

```
export ASAN_OPTIONS="log_path=asan_output.log"
./executable
```

All options that can be set can be found [here](https://github.com/google/sanitizers/wiki/SanitizerCommonFlags) 
___

