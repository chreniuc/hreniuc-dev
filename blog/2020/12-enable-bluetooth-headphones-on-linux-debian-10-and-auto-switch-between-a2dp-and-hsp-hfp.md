---
slug: enable-bluetooth-headphones-on-linux-debian-10-and-auto-switch-between-a2dp-and-hsp-hfp
title: Enable bluetooth headphones on Linux Debian 10 and auto-switch between a2dp and hsp/hfp.
authors: [sebi]
tags: [a2dp, bluetooth, coocheer, headphones, linux]
date: 2020-06-25T19:12
---

# Enable bluetooth headphones on Linux Debian 10 and auto-switch between a2dp and hsp/hfp.

This was tested on Debian 10, running KDE.

I bought the following headphones: [Coocheer ANC8](https://www.emag.ro/casti-audio-coocheer-anc8-active-noise-cancelling-nfc-incarcare-rapida-suport-card-micro-sd-bluetooth-geanta-de-transport-negru-spf000027-b/pd/D9CVXMMBM/ "Coocheer ANC8").

I wanted to use them on a desktop machine running Debian 10 with KDE. Here's what I did:

1. Since my desktop environment had not bluetooth capabilities, I bought the following Bluetooth USB Adapter: [TP-Link UB400](https://www.tp-link.com/uk/home-networking/adapter/ub400 "TP-Link UB400"). Inserting it in one of the USB ports of my machine, KDE detected it instantly and activated the bluetooth capability.

After this single step, I could see my headphones on the list of available bluetooth devices but got the following error when trying to connect to it (seen in journalctl)

`a2dp-sink profile connect failed for 30:21:07:9D:72:05: Protocol not available`

<!--truncate-->

2. I installed the *pulseaudio-module-bluetooth* package.
```bash
apt-get install pulseaudio-module-bluetooth
```

After this (also restart the bluetooth `systemctl restart bluetooth`) I could successfully connect to my headphones and listen to music. The ugly part is that the music sound pretty bad. This was because the connection was using the Bluetooth profile *Headset Head Unit (HSP/HFP)*.
![file](https://www.hreniuc.dev/wp-content/uploads/2020/06/image-1593093955369.png)

To improve the quality of the sound I did the following step:

3. I added the following lines in the */etc/bluetooth/main.conf*:
```bash
[General]
# Automatically connect both A2DP and HFP/HSP profiles for incoming
# connections. Some headsets that support both profiles will only connect the
# other one automatically so the default setting of true is usually a good
# idea.
AutoConnect=true
Enable=Source,Sink,Media,Control,Gateway,Socket,Headset
MultiProfile = multiple
```
This enabled the a2dp profile by default when connecting the headset. This profile provides more qualitative sound. Also, the setting `MultiProfile = multiple` theoretically allows you to use multiple profiles for a device. I want that because the *a2dp* profile while high-quality, it's unidirectional, so the microphone of my headphones will not work. The *HFP/HSP* profile enables both the speakers and the microphone of my headphones.


Ok, so the last thing was to find a way to set the *a2dp* profile as the default one (for high-quality audio) while switching to *HFP/HSP* when I'd like to use my microphone. Fortunately, pulseaudio offers an easy solution to this, by appending `auto_switch=2` when loading the `module-bluetooth-policy`.

4. Modify */etc/pulse/default.pa*:
```bash
#load-module module-bluetooth-policy ORIGINAL LINE
load-module module-bluetooth-policy auto_switch=2
```

After restarting the bluetooth service, I encountered the following error:
> `Failed to register as a handsfree audio agent with ofono: org.freedesktop.DBus.Error.ServiceUnknown: The name org.ofono was not provided by any .service files`

5. Install the ofono package:
```bash
apt-get install ofono
```

Restarting the bluetooth again I was good to go. When listening to music, the profile would be *a2dp* so I have good quality. When some application requests microphone usage, the profile changes to *HFP/HSP* and the microphone is activated.

*Note: You can also commute manually between profiles:* ![file](https://www.hreniuc.dev/wp-content/uploads/2020/06/image-1593095752078.png)

*This is necessary as sometimes, after reconnecting, the headphones won't work and you must switch between profiles to restore them. Also, sometimes the auto-switch will hang, so you must do it manually.*