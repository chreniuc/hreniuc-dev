---
slug: install-from-experimental-or-from-backports-on-debian
title: Install from experimental or from backports on debian
authors: [cristi]
tags: [backports, debian, experimental, package, testing]
date: 2019-06-21T19:19
---

# Install from experimental or from backports on debian

First add these lines to `/etc/apt/sources.list`:

```bash
deb http://deb.debian.org/debian stretch-backports main

deb http://deb.debian.org/debian experimental main
```

Run: 

```bash
apt-get update

apt-get -t stretch-backports install tmux
```