---
slug: install-nodejs-and-npm-on-ubuntu-without-sudo
title: Install nodejs and npm on ubuntu without sudo
authors: [cristi]
tags: [install, no root, no sudo, nodejs, npm, ubuntu]
date: 2018-03-31T21:05
---

# Install nodejs and npm on ubuntu without sudo

Install **nodejs** in a specific path:

```bash
echo 'export PATH=$HOME/programs/bin:$PATH' >> ~/.bashrc
# the `programs` dir should exist, the `bin` will be
# created if it doesn't exist, when you will be running `./configure`
source ~/.bashrc
curl http://nodejs.org/dist/node-latest.tar.gz | tar xz --strip-components=1
./configure --prefix=$HOME/programs # your prefix
make install# --j8
# The `make install` step takes a lot, you should do it on multiple threads using --j option
```

Install **npm** on specific path:

```bash
wget https://registry.npmjs.org/npm/-/npm-5.8.0.tgz # specify what version you want 
tar xzf npm-5.8.0.tgz 
cd package/
node bin/npm-cli.js install -gf --prefix=~/programs/ ../npm-5.8.0.tgz
# use the prefix where node was installed
```

Test them both:

```bash
node -v
#
npm -v
```