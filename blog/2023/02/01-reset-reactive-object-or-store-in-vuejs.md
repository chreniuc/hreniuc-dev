---
slug: reset-reactive-object-or-store-in-vuejs
title: "Reset reactive object or store in vuejs"
authors: [cristi]
tags: [vuejs, typescript, composition api]
date: 2023-02-16T18:01
---

# Reset reactive object or store in vuejs

I was using composition API and I had an object/store that I wanted to reset on client logout. I found that I can do it like this:


```ts
const company = reactive(new Company());

// Used when we re-login or on logout.
export class CompanyUtil {
  public static clear() {
    //https://stackoverflow.com/a/61509432
    // https://github.com/vuejs/core/issues/1081
    Object.assign(company, new Company());
  }
}

export default company;
```
