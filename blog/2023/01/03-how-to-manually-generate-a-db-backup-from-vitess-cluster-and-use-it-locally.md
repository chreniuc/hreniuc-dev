---
slug: how-to-manually-generate-a-db-backup-from-vitess-cluster-and-use-it-locally
title: How to manually generate a db backup from vitess cluster and use it locally
authors: [cristi]
tags: [kubernetes, rke, rke2, hetzner, vitess, backup]
date: 2023-01-05T12:01
---

# How to manually generate a db backup from vitess cluster and use it locally

Install vitess client locally first:

```bash
wget https://github.com/vitessio/vitess/releases/download/v14.0.0/vitess_14.0.0-9665c18_amd64.deb

sudo dpkg -i vitess_14.0.0-9665c18_amd64.deb
```

**Start the port forwarding**

*Note: Make sure you have the `KUBECONFIG` env set when running `pf.sh`*

```bash
cd vitess
bash pf.sh &

alias vtctlclient="vtctlclient -server localhost:15999 -alsologtostderr"
alias mysql="mysql -h 127.0.0.1 -P 15306 -u domain-com_admin"


Pass: `domain-com_admin_`
```

**Connnect to the db to test if it works**

```bash
mysql -pdomain-com_admin_
```

**Create the backup - only the schema**

```bash
mysqldump -d -h 127.0.0.1 -P 15306 -u domain-com_admin -pdomain-com_admin_ domain-com > domain-com-dev-schema.sql
```

**Create the backup - the complete db**

```bash
mysqldump -h 127.0.0.1 -P 15306 -u domain-com_admin -pdomain-com_admin_ domain-com > domain-com-dev.sql
```

**Import the db locally**

**!!Make sure you use another bash terminal, nnot the one you added the aliases!!**

```bash
# Create the database
mysql -u root -proot
create database domain_com_dev;
quit

# Import it
mysql -u root -proot domain_com_dev < domain-com-dev.sql
```

If you encounnter errors, you might have to run these commannds:

```bash
sed -i 's/utf8mb4/utf8/g' domain-com-dev.sql
sed -i 's/utf8_0900_ai_ci/utf8_general_ci/g' domain-com-dev.sql
```

And retry import.