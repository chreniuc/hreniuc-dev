---
slug: gdb-tips
title: GDB Tips
authors: [cristi]
tags: [c++, debugger, gdb]
date: 2020-06-22T19:08
---

# GDB Tips

[GDB cheatsheet](https://darkdust.net/files/GDB%20Cheat%20Sheet.pdf)

Start gdb with coredump:

```bash
gdb ./executable coredump
```

Print all bracktrace:

```bash
bt full
```

Go to a specific frame:

```bash
frame 3
```

Content of an oject of a specific type from a specific address:

```bash
print *(class::client::client_data*)0x7f2a10000c50
```

Content of a variable:

```bash
print conn_ptr
```

Print the content of the object stored in a shared_ptr:

```bash
print *shared_ptr_obj._M_ptr
```

Print the content of std::string:

```bash
# 0x7fdaa000f690 is the address of the string.

print *(char**)0x7fdaa000f690
```

Print a specific element from a vector:

```bash
# A shared_ptr to a vector
print *((*vector_shared_ptr._M_ptr)._M_impl._M_start+0)

print *((vector_obj)._M_impl._M_start+0)
```

Print multiple elements from a vector(ex 9 elements):

```bash
# A shared_ptr to a vector
print *((*vector_shared_ptr._M_ptr)._M_impl._M_start)@9


print *((vector_obj)._M_impl._M_start)@9
```

___

Source: https://stackoverflow.com/questions/6261392/printing-all-global-variables-local-variables

Type `info variables` to list "All global and static variable names".

Type `info locals` to list "Local variables of current stack frame" (names and values), including static variables in that function.

Type `info args` to list "Arguments of the current stack frame" (names and values).

___


Print long string(more [here](https://stackoverflow.com/questions/233328/how-do-i-print-the-full-value-of-a-long-string-in-gdb)):

```bash
set print elements 0
print string
```

Print variable value in a file(more [here](https://stackoverflow.com/questions/5941158/gdb-print-to-file-instead-of-stdout)):

```gdb
set logging on

print string

set logging off

# The content can be found in gdb.txt
# If you want to change the file run:
set logging file my_god_object.log

```
