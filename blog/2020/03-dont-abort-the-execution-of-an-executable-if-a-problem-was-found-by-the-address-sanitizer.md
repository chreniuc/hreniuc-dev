---
slug: dont-abort-the-execution-of-an-executable-if-a-problem-was-found-by-the-address-sanitizer
title: Don't abort the execution of an executable if a problem was found by the address sanitizer
authors: [cristi]
tags: [address sanitizer, c++, don't abort when problem was found, linux]
date: 2020-05-14T19:03
---

# Don't abort the execution of an executable if a problem was found by the address sanitizer

Build project with : `-fsanitize=address -fsanitize-recover=address`


Run the excutable with this enviroment variable: `ASAN_OPTIONS=halt_on_error=0`.  Source: https://github.com/google/sanitizers/wiki/AddressSanitizer.

Ex:

```bash
ASAN_OPTIONS=halt_on_error=0 ./executable
```

When the address sanitizer will find a problem, it will not abort the execution of the exe as it ussualy does.