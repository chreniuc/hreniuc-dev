---
slug: color-themes-for-terminal
title: Color themes for terminal
authors: [cristi]
tags: [bash, color, debian, linux, lxde, themes, xfce-terminal]
date: 2019-06-21T19:17
---

# Color themes for terminal

First install `xfce4-terminal`(I had some problems with `lxterminal`):

```bash
apt-get install xfce4-terminal
```

The go here: https://github.com/chriskempson/base16-shell

Follow the steps.

I used `base16_flat` theme.