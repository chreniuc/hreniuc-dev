---
slug: create-an-endpoint-in-kubernetes-to-connect-to-an-external-service-mariadb
title: Create an endpoint in kubernetes to connect to an external service - mariadb
authors: [cristi]
tags: [kubernetes, cluster, endpoint, external, mariadb ]
date: 2023-01-06T07:01
---

# Create an endpoint in kubernetes to connect to an external service(mariadb)

Check [this](https://rtfm.co.ua/en/kubernetes-what-is-endpoints/#Custom_Endpoint)

This is an example of how to connect an internal pod to an external db that is not inside the cluster. In the pod configuration you will need to connect to the `brandName-database` service from below.

```yml
apiVersion: v1
kind: Service
metadata:
  name: brandName-database
spec:
  ports:
    - name: brandName-database-external
      protocol: TCP
      port: 3306
      targetPort: 3306

---
kind: Endpoints
apiVersion: v1
metadata:
  name: brandName-database
subsets:
  - addresses:
      - ip: 192.168.100.52
    ports:
      - port: 3306
        name: brandName-database-external
```