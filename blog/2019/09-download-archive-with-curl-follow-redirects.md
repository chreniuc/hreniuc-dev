---
slug: download-archive-with-curl-follow-redirects
title: Download archive with curl, follow redirects
authors: [cristi]
tags: [linux, how to, curl]
date: 2019-03-28T19:09
---

# Download archive with curl, follow redirects

This is how you can download an archive using curl:

```bash
curl -L -O https://github.com/prometheus/mysqld_exporter/releases/download/v0.11.0/mysqld_exporter-0.11.0.linux-amd64.tar.gz
```

 - `-L` - Follow redirects
 - `-O` - Write output to a file named as the remote file