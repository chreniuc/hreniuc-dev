---
slug: automatically-sync-all-folders-in-thunderbird
title: Automatically sync all folders in Thunderbird
authors: [cristi]
tags: [how to, thunderbird]
date: 2020-02-14T19:01
---

# Automatically sync all folders in Thunderbird


Start up Thunderbird, open the Config Editor (Tools -> Options -> Advanced -> General -> Config Editor), and change the mail.server.default.check_all_folders_for_new setting to true.

Or in newer versions: Thunderbird menu -> Preferences -> Preferences -> Advanced -> General -> Config Editor

Source: [here](https://tech.tiefpunkt.com/2014/08/automatically-sync-all-folders-in-thunderbird/)