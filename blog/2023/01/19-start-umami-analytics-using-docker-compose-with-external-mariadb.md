---
slug: start-umami-analytics-using-docker-compose-with-external-mariadb
title: "Start umami analytics using docker compose with external mariadb"
authors: [cristi]
tags: [umami, docker, docker-compose, analytics, mariadb]
date: 2023-01-20T20:01
---

# Start umami analytics using docker compose with external mariadb

[Umami](https://umami.is/docs/getting-started) is a lightweight alternative to google analytics.

You will need a db and an user: `umami` db and `umami-user`.

```sql
create database umami;


CREATE USER 'umami-user'@'%' IDENTIFIED BY 'umami-pass';

GRANT ALL PRIVILEGES ON `umami`.* TO 'umami-user'@'%';

mysql -uroot -p umami < schema.sql
```

<!-- truncate -->

The db should be initialized with the [schema.mysql.sql](https://github.com/umami-software/umami/blob/master/sql/schema.mysql.sql).

Docker compose should look like this:

```yml
version: '3'
services:
  umami_prod:
    container_name: umami_prod
    image: ghcr.io/umami-software/umami:mysql-v1.39.5
    port:
      - "3000:3000"
    environment:
      DATABASE_URL: mysql://UMAMI_DB_USER:UMAMI_DB_PASS@DB_HOST:3306/DATABASE_NAME
      DATABASE_TYPE: mysql
      HASH_SALT: random_values_sdfasdasfdsaf
      COLLECT_API_ENDPOINT: "/api/collect"
      TRACKER_SCRIPT_NAME: "umami-script-name"
    restart: always
```

```bash
docker compose up
```

You can access the web interface of umami on [http://localhost:3000/dashboard](http://localhost:3000/dashboard).

These two variables are important:

 - COLLECT_API_ENDPOINT: "/api/collect"
 - TRACKER_SCRIPT_NAME: "umami-script-name"

If you do not set them, umami might be blocked by the adblockers on your website.

When you want to monitor a website you will have to add something like this to your website(in `<head>` tag):

```html
<script async defer data-website-id="b3bf2858-0b02-4556-afb2-5b9403289f1a" src="http://localhost:3000/umami-script-name.js"></script>
```

Check docs for:

 - [Umami](https://umami.is/docs/about)
 - [Env variables](https://umami.is/docs/environment-variables)