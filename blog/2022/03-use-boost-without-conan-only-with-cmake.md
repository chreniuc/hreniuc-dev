---
slug: use-boost-without-conan-only-with-cmake
title: Use boost without conan, only with cmake
authors: [cristi]
tags: [c++, boost, cmake, conan]
date: 2022-04-14T19:03
---

# Use boost without conan, only with cmake

I've found [this](https://www.reddit.com/r/cpp/comments/u2sq99/comment/i4nv7jl/?utm_source=share&utm_medium=web2x&context=3). I haven't tried it, but I want to save it just in case.

Contents:

Boost has experimental CMake support. You have to do a bit of dev to get it working but we've had good experiences so far.

```cmake
set(BOOST_INCLUDE_LIBRARIES system thread) # enabled libraries
set(BOOST_ENABLE_CMAKE ON) # CMake support
FetchContent_Declare(boost GIT_REPOSITORY https://github.com/boostorg/boost.git ...
```

The above will get you the compiled libs working. For the header library (`Boost::boost` aka `Boost::headers`) there doesn't seem to be CMake support as yet so we wrote a few lines of CMake code to iterate through the `FetchContent` source directory and added all include folders to a new `IMPORTED INTERFACE` target.

