---
slug: linux-fresh-install-ubuntu
title: Linux fresh install (Ubuntu)
authors: [cristi]
tags: [linux, ubuntu]
date: 2022-05-11T19:05
---

# Linux fresh install (Ubuntu)

Install the c++ compiler and the other things:

```bash
sudo apt-get install build-essential curl
```

*Note: You should also check this post: [Preparing C++ Dev infrastructure](preparing-c-dev-infrastructure)*

Tmux and tmuxinator:

```bash
sudo apt-get install tmux tmuxinator
```


Others:

```bash
sudo apt-get install liblz4-tool

# To uncompress a lz4
lz4 -dc --no-sparse archive.tar.lz4 | tar xf -

sudo apt-get install vim
```

```bash
sudo apt-get install sqlitebrowser
```

**MariaDB**

```bash
sudo apt update
sudo apt install mariadb-server
sudo mysql_secure_installation
sudo mariadb

GRANT ALL PRIVILEGES on *.* to 'root'@'localhost' IDENTIFIED BY 'root';
FLUSH ALL PRIVILEGES;
```

**Docker:**

[Source](https://docs.docker.com/engine/install/ubuntu/)

```bash
sudo apt-get update

 sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
 
 echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


 sudo apt-get update
 sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

sudo usermod -aG docker $USER
```

**Kubernetes utils:**

[Source](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

```bash
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl

sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt-get update
sudo apt-get install -y kubectl
```

From snap:

 - spotify
 - vscode(you can download it from ms website directly)
 - dbeaver
 - 
 
