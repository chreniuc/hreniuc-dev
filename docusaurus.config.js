// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Hreniuc\'s Library',
  tagline: 'Shelves of knowledge',
  url: 'https://hreniuc.dev',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'chreniuc', // Usually your GitHub org/user name.
  projectName: 'hreniuc.dev', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: false,
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/chreniuc/hreniuc-dev/-/tree/master/',
          blogSidebarCount: 10,
          routeBasePath: '/'
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        sitemap: false,
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Hreniuc\'s Library',
        logo: {
          alt: 'Hreniuc\'s Library Logo',
          src: 'img/logo.svg',
        },
        items: [
          {to: '/archive', label: 'Archive', position: 'left'},
          {
            to: '/books',
            position: 'left',
            label: 'Books',
          },
          {
            to: '/tags',
            position: 'left',
            label: 'Tags',
          },
          {
            to: 'https://www.linkedin.com/in/cristian-hreniuc-70117413a/',
            position: 'left',
            label: 'LinkedIn',
          },
          {
            to: '/about',
            position: 'right',
            label: 'About',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Community',
            items: [
              {
                label: 'Linkedin',
                href: 'https://www.linkedin.com/in/cristian-hreniuc-70117413a/',
              },
              {
                label: 'Github',
                href: 'https://github.com/chreniuc',
              },
              {
                label: 'Gitlab',
                href: 'https://gitlab.com/chreniuc',
              }
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Hreniuc\'s Library, Inc. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
  themes: [
    // ... Your other themes.
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      {
        // ... Your options.
        // `hashed` is recommended as long-term-cache of index file is possible.
        hashed: true,
        indexDocs: false,
        indexBlog: true,
        blogRouteBasePath: "/",
        // For Docs using Chinese, The `language` is recommended to set to:
        // ```
        // language: ["en", "zh"],
        // ```
      },
    ],
  ],
};

module.exports = config;
