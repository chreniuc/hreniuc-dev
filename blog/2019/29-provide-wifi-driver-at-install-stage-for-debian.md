---
slug: provide-wifi-driver-at-install-stage-for-debian
title: Provide wifi driver at install stage for Debian
authors: [cristi]
tags: [debian, install debian buster, wifi driver]
date: 2019-09-03T19:29
---

# Provide wifi driver at install stage for Debian

Ever encountered this type of error when you were trying to install Linux on your Laptop?

```bash
Some of your hardware needs non-free firmware files to operate.  The firmware can be loaded from
removable media, such as a USB stick or floppy.

The missing firmware files are: iwlwifi-6000g2a-6.ucode iwlwifi-6000g2a-5.ucode

If you have such media available now, insert it, and continue.
```

Here's how to fix it, based on steps from [this guy](https://www.linuxquestions.org/questions/debian-26/how-to-provide-non-free-firmware-files-to-the-debian-jessie-installer-4175542680/).

If you fot to this ponit in the installation process, do not close the  installation.

Get another usb stick and format it using `FAT`.

##### Plug in the usb

If you already have a usb that is formated as `FAT`(it doesn't need to be empty), skip the next two steps.

##### Get the usb device:
```bash
fdisk -l
# Output
Disk /dev/sda: 119.2 GiB, 128035676160 bytes, 250069680 sectors
Disk model: SanDisk SD8SBAT1
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x995ebae8

Device     Boot Start       End   Sectors   Size Id Type
/dev/sda1  *     2048 250068991 250066944 119.2G 83 Linux


Disk /dev/sdb: 931.5 GiB, 1000204886016 bytes, 1953525168 sectors
Disk model: WDC WD10EZEX-75W
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: 08BAF1CD-519D-4758-8BFE-6B50CBBC078D

Device          Start        End    Sectors   Size Type
/dev/sdb1  1922275328 1953523711   31248384  14.9G Linux swap
/dev/sdb2        2048 1922275327 1922273280 916.6G Linux filesystem

Partition table entries are not in disk order.


Disk /dev/sdc: 29.3 GiB, 31457280000 bytes, 61440000 sectors
Disk model: ProductCode     
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x263d4ee4

Device     Boot Start      End  Sectors  Size Id Type
/dev/sdc1  *     2048 61439999 61437952 29.3G 83 Linux
```

##### Delete all partitions and create a new FAT one:

```bash
fdisk /dev/sdc

# Press 'd' untill you deleted all partitions.

# Press 'w' to write the changes.

fdisk /dev/sdc
# Press 'n' to create a new partition

# Press 'w' to write the changes.

# Format it as FAT:

mkfs.fat /dev/sdc1
```
##### Download the drivers and copy them on the drive:

Go to https://packages.debian.org/buster/firmware-iwlwifi and download the deb file.

Extract it's content:

```bash
mkdir wifi
dpkg-deb --extract firmware-iwlwifi_20190114-1_all.deb wifi
```

Copy files to usb:

```bash
cp -r wifi/lib/firmware /mnt/usb_stick
```

##### Plugin the usb stick in the laptop that you are installing Debian and mount it:

 - Plugin the USB stick.

 - Press `<CTRL>+<Alt>+<F2>` key combination, to switch to the second virtual terminal.

 - Run the `blkid` command, to find out under which device name the USB stick was known: /dev/sdc. Its FAT filesystem, which contained the firmware files, then, was /dev/sdc1.

 - Mount the usb stick:
 
 ```bash
 cd /mnt
 mkdir mount_usb
 mount /dev/sdc1 /mnt/mount_usb
 ```
 
 - Create a symbolic link to the `firmware` folder from the usb stick in `/lib/firmware`:
 
 ```bash
 ln -s /mnt/mount_usb/firmware /lib/firmware
 ```


 - Press `<CTRL>+<Alt>+<F5>` to continue the installation process.

Everything should work out of the box now.