---
slug: force-gdb-from-qtcreator-to-ignore-capturing-a-signal
title: Force GDB from qtcreator to ignore capturing a signal
authors: [cristi]
tags: [debugger, gdb, Qtcreator, signal, c++]
date: 2020-07-02T19:13
---

# Force GDB from qtcreator to ignore capturing a signal

Source: https://stackoverflow.com/a/19537899

My server(started from qtcreator in Debug) was sending data to a client and the client crashed ungracefully(CTRL+C), this resulted in two things:
 - SIGPIPE signal was generated(which was handled in my code, but before getting to my code GDB was handleling it, and crashed my server)
 - The server crashed with SEGFAULT when I was running it in terminal with no GDB attached.
 
So, to investigate the `SEGFAULT` easier, inside QtCreator, I had to force GDB to ignore the first signal `SIGPIPE`, I did this by doing thse two steps:
  - uncheck the box "Show a message box when receiving a signal" in Tools -> Options... -> Debugger -> GDB.
  -  If you want gdb not to catch a signal but pass it to your program, you need to give gdb a command like the following:` handle SIGPIPE pass nostop noprint `(see` Window -> Views -> Debugger Log` or `Tools -> Options... -> Debugger -> GDB -> GDB startup script Debugging Helper Customization` or `Tools -> Options... -> Debugger -> GDB -> Locals & Expressions` (like the image below for the SIGPIPE signal)).

<!--truncate-->
![](qt-creator-gdb-signal.png)