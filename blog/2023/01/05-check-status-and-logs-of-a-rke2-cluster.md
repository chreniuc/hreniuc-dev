---
slug: check-status-and-logs-of-a-rke2-cluster
title: Check status and logs of a RKE2 cluster
authors: [cristi]
tags: [kubernetes, rke, rke2, status, logs ]
date: 2023-01-05T14:01
---

# Check status and logs of a RKE2 cluster

This document documents useful steps to investigate when we are having problems with our engine cluster(RKE2).

## 1. Status of rke2

```bash
ssh ansible_noob@SERVER-IP-1
sudo su
# Server
systemctl status rke2-server
# Agent
systemctl status rke2-agent
```

Config file: `less /etc/rancher/rke2/config`

## 2. Check the logs of rke2

[Source](https://docs.rke2.io/install/quickstart/#4-follow-the-logs-if-you-like)

Server logs:

```bash
ssh ansible_noob@SERVER-IP-1
sudo su
journalctl -u rke2-server -f
```

Agent logs:

```bash
ssh ansible_noob@SERVER-IP-2
sudo su
journalctl -u rke2-agent -f
```
