---
slug: find-and-install-libraries-for-x86-on-linux-debian
title: Find and install libraries for x86 on linux, Debian
authors: [cristi]
tags: [debian, i386, install, Kali, libraries, linux, x86]
date: 2018-11-25T21:13
---

# Find and install libraries for x86 on linux, Debian

Add the new architecture: `sudo dpkg --add-architecture i386`Update the package database: `sudo apt-get update`Install the libraries that you want: `apt-get install libexpat1:i386`