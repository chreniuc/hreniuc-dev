---
slug: how-to-redirect-youtube-home-page-to-subscriptions
title: How to redirect YouTube home page to Subscriptions
authors: [cristi]
tags: [how to, recommended, redirect, youtube]
date: 2018-03-03T19:01
---

# How to redirect YouTube home page to Subscriptions

Use [this](https://addons.mozilla.org/en-US/firefox/addon/ytmysubs/) addon on firefox.

> YouTube\'s new design uses Polymer 2.0, and the userscript below no longer works. Since as of right now this question is the top Google hit for queries related to changing this behavior I have created a [Chrome extension](https://chrome.google.com/webstore/detail/replace-youtubes-home-wit/nfffnooajndeeejgejfkbphjocpkblog) and a [Firefox 57+ add-on](https://addons.mozilla.org/en-US/firefox/addon/ytmysubs/) which I [open sourced](https://github.com/SeinopSys/YTMySubs-chrome) in case anyone wants to look at the internals (or create a port for other browsers). Please note that **I will no longer be maintaining the userscript version.**

Source: [stackexchange](https://webapps.stackexchange.com/questions/44271/how-to-set-my-subscriptions-as-the-default-view-on-youtube-com?rq=1)