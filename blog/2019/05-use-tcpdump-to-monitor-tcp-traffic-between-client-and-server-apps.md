---
slug: use-tcpdump-to-monitor-tcp-traffic-between-client-and-server-apps
title: Use tcpdump to monitor tcp traffic between client and server apps
authors: [cristi]
tags: [lsof, tcp traffic, tcpdump, linux]
date: 2019-02-14T19:05
---

# Use tcpdump to monitor tcp traffic between client and server apps

On the server side:
```bash
tcpdump -n -i interface -n "src host client-host.com and dst port server-port"
```

You can get the client host using `lsof`:

```bash
lsof -itcp -a -p server_pid
```

This will print all active tcp connections to the server app with the specific pid.

On the client side:
```bash
tcpdump -n -i interface -n "dst host server-host.com and dst port server-port"
```

`interface` can be taken via `ip addr`