---
slug: vue-js-tips-and-tricks
title: Vue.js Tips and Tricks
authors: [cristi]
tags: [tips, tricks, vue.js, vuejs]
date: 2018-07-07T21:07
---

# Vue.js Tips and Tricks

**LE: 14th Sept 2022 - Use Vue3 and you no longer need actions and mutations. Use [State Management with Reactivity API](https://vuejs.org/guide/scaling-up/state-management.html)**

**1.** When you want to commit to a state(using `actions` and `mutations`), do not pass multiple parameters to the `commit` method, they will have the value undefined. Create an object and pass it like that. This:

```js
commit(types.UPDATE_JOB, index, title);
```

Will become this:

```js
commit(types.UPDATE_JOB, {index: index, title: job.title});
```

- - - - - -

**2.** Props that have default value:

```js
export default {
  props: {
    prop_without_defaut_value: {},
    prop_with_defaut_value: {
      default: false // this is a bool value
    },
    prop_with_defaut_value_string: {
     default: String 
    }
  },
  data() {
    return {
      var_name: false
    }
  }
}
```

- - - - - -

**3.** Use `mapActions` and custom methods:

```js
methods: Object.assign({},
  /* If we want to use mapActions and other
  custom methods this is the way to do it
  otherwise it will give an error */
  mapActions([
    'action_1',
    'action_2'
  ]), {
    method_1() {
      this.title = this.job.title;
      this.edit_mode = !this.edit_mode;
    },
    method_2() {}
  }
),
```