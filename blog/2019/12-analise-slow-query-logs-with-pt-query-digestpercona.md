---
slug: analise-slow-query-logs-with-pt-query-digestpercona
title: Analise slow query logs with pt-query-digest(percona)
authors: [cristi]
tags: [analise slow queries, mariadb, mysql, percona, pt-query-digest, slow queries, sql]
date: 2019-04-03T19:12
---

# Analise slow query logs with pt-query-digest(percona)

First we need to install everything, [here](https://www.percona.com/doc/percona-repo-config/percona-release.html#installation) are the steps:

*These steps are for centos*
First step is to install `percona-release`:

```bash
yum install https://repo.percona.com/yum/percona-release-latest.noarch.rpm
```

Enable the package that contains the `pt-query-digest` tool:
```bash
percona-release enable tools
```

Next install `percona-toolkit`:

```bash
yum install percona-toolkit
```

How to use `pt-query-digest`:

```bash
pt-query-digest /../mysql_slow/slow-query.log
```

Set interval:

```bash
pt-query-digest --since '2019-02-07 08:09:00' --until '2019-02-07 08:02:03' /../mysql_slow/slow-query.log > output.out
```

Filter host, database or other things:

```bash
# Ignore queries from a specific database: ($event->{db} || "") && $event->{db} ne "ignored_schema"
# Ignore queries are not on a database:" ($event->{db} || "") , Ex PING
#Ignore queries that came from a specific IP address: ($event->{host} || "") && $event->{host} ne "192.168.2.1"
pt-query-digest --filter  '($event->{db} || "") && $event->{db} && $event->{db} ne "ignored_schema" && $event->{cmd} ne "Admin" && ($event->{host} || "") && $event->{host} ne "192.168.2.1" && $event->{host} ne "192.168.0.1"' /../mysql_slow/slow-query.log > output.out
```

To see all components of `event` run this command:

```bash
# This will print the $event variable with all it's componets:

pt-query-digest pdbx5-slow-query.log  --filter  '($event->{db} || "") && $event->{db} && $event->{db} ne "ignored_database" && print Dumper $event'  --no-report  --sample 1


```

Uninstall:

```bash
yum remove percona-toolkit

percona-release disable tools

yum remove percona-release
```