---
slug: fix-404-not-found-in-ubuntu-for-eol-version
title: Fix 404 not found in Ubuntu, for EOL version
authors: [cristi]
tags: [eol, ubuntu, linux]
date: 2020-12-14T19:19
---

# Fix 404 not found in Ubuntu, for EOL version

I've changed in `/etc/apt/sources.list` from:

```
deb-src http://ro.archive.ubuntu.com/ubuntu/ eoan universe
```

to :

```
deb mirror://mirrors.ubuntu.com/mirrors.txt eoan universe
```

If this happens, it means that the best solution is to upgrade your version to a LTS.