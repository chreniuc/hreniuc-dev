---
slug: useful-things-for-coredumps-on-linux
title: Useful things for coredumps on linux
authors: [cristi]
tags: [coredump, linux, location of coredumps]
date: 2020-03-06T19:02
---

# Useful things for coredumps on linux

Where are the coredumps generated?

```bash
$ cat /proc/sys/kernel/core_pattern

/tmp/core-%e.%p.%h.%t
```

Before running your executable, set the limit for the coredump:

```bash
$ ulimit -c unlimited
```

You can add `ulimit -n 99999` in your `~/.bashrc`. But if you want all gui apps(qtcreator) to use it, you should also include it in your `~/.xsession`:

```bash
#!/bin/bash

ulimit -n 99999
```

You mai also need to update the limits(only if setting the limit returns error):

```bash
sudo nano /etc/security/limits.conf

*               hard    nofile        99999

# --------------------

sudo nano /etc/pam.d/common-session

session required pam_limits.so
```