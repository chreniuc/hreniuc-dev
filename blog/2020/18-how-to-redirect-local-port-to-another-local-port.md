---
slug: how-to-redirect-local-port-to-another-local-port
title: How to redirect local port to another local port
authors: [cristi]
tags: [linux, local, port, redirect, socat]
date: 2020-11-17T19:18
---

# How to redirect local port to another local port

This can be done using `socat`:

```bash
socat TCP-LISTEN:5000,fork TCP:localhost:9000
```

This redirects all tcp connections from port `5000` to `9000`. To run this command, the `5000` port shouldn't be already in use.