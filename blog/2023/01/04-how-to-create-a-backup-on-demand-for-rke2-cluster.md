---
slug: how-to-create-a-backup-on-demand-for-rke2-cluster
title: How to create a backup on-demand for RKE2 cluster
authors: [cristi]
tags: [kubernetes, rke, rke2, hetzner, backup ]
date: 2023-01-05T13:01
---

# How to create a backup on-demand for RKE2 cluster

This doc describes how to create a backup on-demand for RKE2 cluster. We already have set up a recurring backup, from the rke2 engine. But if we ever want to backup this, before an update or something, we should follow these steps.


[Source](https://docs.rke2.io/backup_restore/)

Atm only the snapshot with config file works, there is a bug in their server:
 - https://github.com/rancher/rke2/issues/2103
 - https://github.com/rancher/rke2/issues/2121

```bash
rke2 etcd-snapshot --config /etc/rancher/rke2/config2.yaml --debug
```

```yaml
s3: true
s3-access-key: keyId
s3-bucket: domain_com-contabo-rke
s3-endpoint: s3.eu-central-003.backblazeb2.com
s3-region: eu-central-003
s3-secret-key: applicationKey
snapshot-compress: true
```

This will create a snapshot and upload it to S3(BackBlaze).

## Not working atm

```bash
rke2 etcd-snapshot \
  --snapshot-compress \
  --s3 \
  --s3-endpoint "s3.eu-central-003.backblazeb2.com" \
  --s3-bucket "domain_com-contabo-rke" \
  --s3-access-key "keiId" \
  --s3-secret-key "applicationKey"
```