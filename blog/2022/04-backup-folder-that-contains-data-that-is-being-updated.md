---
slug: backup-folder-that-contains-data-that-is-being-updated
title: Backup folder that contains data that is being updated
authors: [cristi]
tags: [how to, linux, backup]
date: 2022-04-29T19:04
---

# Backup folder that contains data that is being updated

I usually have a `data` folder in my home folder, where I keep all my data, including the cache of my browser.

When I switch PCs I have to copy that folder on my new pc, if I try to archive the folder it will create a corupted compressed archive or the compression will fail.

So, to fix this I had to follow the following steps:

```bash

# Create a copy of the folder with rsync
# Will copy all folders/files from `data` inside the `data_backup` folder
sudo rsync -a --info=progress2 /home/chreniuc/data/ /home/chreniuc/data_backup/

# Create an archive
tar -cf - data_backup | lz4 -c > data_backup_29.04.2022.tar.lz4

# Copy the archive where you want

cp data_backup_29.04.2022.tar.lz4 destination

# To uncompress a lz4
lz4 -dc --no-sparse data_backup_29.04.2022.tar.lz4 | tar xf -
```