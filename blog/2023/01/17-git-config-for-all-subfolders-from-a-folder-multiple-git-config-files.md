---
slug: git-config-for-all-subfolders-from-a-folder-multiple-git-config-files
title: "Git config for all subfolders from a folder - multiple git config files"
authors: [cristi]
tags: [git, gitconfig]
date: 2023-01-18T20:01
---

# Git config for all subfolders from a folder - multiple git config files

This is a tutorial on how to set a git config for a specific folder where you have work related projects and a folder where you have personal projects.

### Create a gitconfig file for your personal projects

Contents of th:

```bash
# In ~/data/personal/configs/git/.personal_gitconfig
[user]
    name = Hreniuc Cristian-Alexandru
    email = email@gmail.com
```

Add the path to that config in the global git config:

```bash
[includeIf "gitdir:~/data/personal/"]
    path = ~/data/personal/configs/git/.personal_gitconfig
```

**Make sure you add it at the end of `.gitconfig` and make sure the last `\` from the `If` exists, otherwise, it won't work.**

[Source](https://alysivji.github.io/multiple-gitconfig-files.html)