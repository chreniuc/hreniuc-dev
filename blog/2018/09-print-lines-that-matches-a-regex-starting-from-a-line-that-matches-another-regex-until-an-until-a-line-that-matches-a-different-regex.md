---
slug: print-lines-that-matches-a-regex-starting-from-a-line-that-matches-another-regex-until-an-until-a-line-that-matches-a-different-regex
title: Print lines that matches a regex starting from a line that matches another regex until an until a line that matches a different regex
authors: [cristi]
tags: [awk, linux, print lines after match, print lines before match, print lines matching regex]
date: 2018-07-10T21:08
---

# Print lines that matches a regex starting from a line that matches another regex until an until a line that matches a different regex

Print lines that matches a regex starting from a line that matches another regex until an until a line that matches a different regex:

```bash
cat file | awk '/startling_regex_excluded/{next} /printable_regex/{print} /until_regex/{exit}'
```

Having this file:

```bash
text
text2
startling_regex_excluded
printable_regex
printable_regex
printable_regex
until_regex
test
printable_regex
printable_regex
```

The output will be:

```bash
printable_regex
printable_regex
printable_regex
```

[AWK CheatSheet](https://www.shortcutfoo.com/app/dojos/awk/cheatsheet)

### Get all lines starting from a position:

```sh
awk '/start line/{flag=1;next}flag' file.log
```
