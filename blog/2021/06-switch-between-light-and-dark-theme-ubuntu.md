---
slug: switch-between-light-and-dark-theme-ubuntu
title: Switch between light and dark theme - Ubuntu
authors: [cristi]
tags: [dark, gsettings, light, switch theme, ubuntu]
date: 2021-11-15T19:06
---

# Switch between light and dark theme - Ubuntu

Using gsettings:

Light:

```bash
gsettings set org.gnome.desktop.interface gtk-theme Yaru-light
```

Dark:

```bash
gsettings set org.gnome.desktop.interface gtk-theme Yaru-dark
```