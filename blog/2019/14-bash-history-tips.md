---
slug: bash-history-tips
title: Bash History Tips
authors: [cristi]
tags: [bash, bashrc, linux, history]
date: 2019-05-28T19:14
---

# Bash History Tips

Add this to `~/.basrc`:

```bash
# Display date of the command
export HISTTIMEFORMAT="%d/%m/%y %T "

# Size(nr of lines) of the history loaded in memory?
export HISTFILESIZE=10000000
# Size(nr of lines) of the history saved in file?
export HISTFILESIZE=10000000
# Path to history file.
export HISTFILE=/home/chreniuc/data/configs/.bash_history
```