---
slug: tmux-tips
title: Tmux tips
authors: [cristi]
tags: [linux, tips, tmux]
date: 2020-06-22T19:09
---

# Tmux tips

Attach to the same session but in a different window:

```
# work_session session should exits
tmux new-session -t 'work_session'
```

Everything about tmux: https://leanpub.com/the-tao-of-tmux/read#window-layouts

Tmuxer: https://github.com/tjhop/tmuxer
