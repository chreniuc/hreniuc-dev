---
slug: auto-update-pwa-created-in-vuejs-with-quasar-without-client-confirmation
title: "Auto-update PWA created in vue.js with quasar without client confirmation"
authors: [cristi]
tags: [vue, pwa, vuejs, vue.js, quasar]
date: 2023-01-19T20:01
---

# Auto-update PWA created in vue.js with quasar without client confirmation

If you have a PWA, you might want to make sure that the client is using the latest version of it. Here are the steps needed to do this using [quasar framework](https://quasar.dev/):


**1. Make sure that the `service-worker.js` is not cached by nginx**

Add this configuration in your nginx:

```bash
location ~ service-worker.js$ {
    #add_header X-debug-whats-going-on 3;
    add_header Cache-Control "no-cache, no-store";
    expires -1;
    try_files $uri $uri/ =404;
    #/service-worker.js; -> https://serverfault.com/a/505099
}
```

<!-- truncate -->

**2. Add `skipWaiting` and `clientsClaim` in `workboxOptions` node, from `pwa` in `quasar.conf.js`**

```js
pwa: {
    workboxPluginMode: "GenerateSW", // 'GenerateSW' or 'InjectManifest'
    // https://quasar.dev/quasar-cli-webpack/developing-pwa/configuring-pwa#reload-and-update-automatically
    workboxOptions: {
    skipWaiting: true,
    clientsClaim: true
    },
```

[Source](https://quasar.dev/quasar-cli-webpack/developing-pwa/configuring-pwa#reload-and-update-automatically)

**3. Call `location.reload()` inside `updated()` method from `src-pwa/register-service-worker.js`**


```js
  updated (/* registration */) {
    console.log("New content is available; please refresh.")
    location.reload()
  },
```

[Source1](https://github.com/quasarframework/quasar/discussions/13176), [source2](https://quasar.dev/quasar-cli-webpack/developing-pwa/handling-service-worker)