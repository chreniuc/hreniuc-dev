---
slug: make-the-text-sharper-on-ubuntu
title: Make the text sharper on Ubuntu
authors: [cristi]
tags: [ubuntu, linux, sharper text, eye care]
date: 2022-09-21T21:17
---

# Make the text sharper on Ubuntu

Add in `~/.fonts.conf` the following contents:

```xml
<?xml version="1.0" ?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
   <fontconfig>
      <match target="font">
         <edit name="autohint" mode="assign">
            <bool>true</bool>
         </edit>
      </match>
</fontconfig>
```

This will help you with your eyes.

[Source](https://superuser.com/a/8868)