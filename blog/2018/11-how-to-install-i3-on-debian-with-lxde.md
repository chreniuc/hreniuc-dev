---
slug: how-to-install-i3-on-debian-with-lxde
title: How to install i3 on debian with LXDE/XFCE lxde, openbox
authors: [cristi]
tags: [debian, dmenu, i3, lxde, xfce]
date: 2018-10-22T21:11
---

# How to install i3 on debian with LXDE/XFCE

**LE: 14th Sept 2022 - Use [regolith-linux](https://regolith-desktop.com/) instead or use Ubuntu and use the default keybindings that support some tilling.**

1. First step, you have to install `i3``apt-get install i3`2. Go to `LXpanel menu > Preferences > Default application for LXSession` then go to tab `Core applications` and set to Window Manager: `i3`3. Edit the autostart script by adding `--desktop-off` in the file `~/.config/lxsession/LXDE/autostart` to the `pcmanfm` command, it should look like this after you edit it: `@pcmanfm --desktop --profile LXDE`. If the `dmenu` doesn\'t find your local binaries(the folders you added to the `$PATH`) you should create a `~/.xsessionrc` and export the `$PATH` env variable there:

```bash
#!/bin/bash
# Content of ~/.xsessionrc


export PATH=${PATH}:${HOME}/.local/bin
```

 **Note: On archlinux + lxde I added that export in .xinitrc** I was having some problems with `lxpanel`, it was taking too much resources so a I deactivated. I commented the line `@lxpanel --profile LXDE` from file `~/.config/lxsession/LXDE/autostart`. #### Debian + xfce: I followed these steps: https://feeblenerd.blogspot.com/2015/11/pretty-i3-with-xfce.html ```bash apt-get install i3 ``` Deactivated `xfwm4` and `xfdesktop` from : `\'Session and Startup\' -> \'Session\' tab.`. Added `i3` in `\'Application Autostart\'` tab. Removed all keybindings. Reboot.