---
slug: solved-bless-hex-editor-there-is-not-enough-free-space-on-the-device-to-save-file
title: Solved Bless Hex Editor - There is not enough free space on the device to save file
authors: [cristi]
tags: [bless, config, Hex Editor, linux, ubuntu]
date: 2018-01-11T13:00
---

# [Solved] Bless Hex Editor - There is not enough free space on the device to save file

The xml config file isn’t closed correctly and the parsing of the parameters doesn’t work. There is a method around it:

 - Edit the file: **/home/user/.config/bless/preferences.xml**
    
    ```xml
    <preferences>
        <pref name="ByteBuffer.TempDir">/tmp</pref> <!-- add this -->
        ...
        <pref name="Default.Layout.File"/>
    </preferences> <!-- Add this -->
    ```

    And make that file **readonly**, this way it won't get modified each time you restart **bless**.

Note: You won't be able to modify the preferences from the **bless** editor, you will have to edit the config file by yourself.

Another method is to rebuild Bless, applying a patch: https://bugs.launchpad.net/ubuntu/+source/bless/+bug/1622951
